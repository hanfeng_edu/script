#!/usr/bin/env python3
import requests
import json
import sys
import os
import re
import time
# import configparser

headers = {'Content-Type': 'application/json;charset=utf-8'}
time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

log_file = "/var/log/zabbix/zabbix_script_dingding.log"
api_url = "https://oapi.dingtalk.com/robot/send?access_token=fe122e1f8586f8a79e7f2de27fdd0c45aa46698121bd7a57ce9e981e7fb4404e"


def log(info):
    try:
        f = open(log_file, 'a+')
        f.write(info)
        f.close()
    except IOError:
        print("Error: 没有找到文件或读取文件失败")


def info_format():
    new_info = ""
    temp_info = str(sys.argv[3]).split('\n')
    print(temp_info)
    for i in temp_info:
        new_info += "- " + str(i) + "\n>"
        print(new_info)
    return new_info


def send_msg(mobiles, title, message):
    json_text = {
        "msgtype": "markdown",
        "markdown": {
            "title": title,
            "text": "#### " + title + "\n> " + message + "\n " + "- " + "@" + mobiles
        },
        "at": {
            "atMobiles": [
                mobiles
            ],
            "isAtAll": False
        }
    }

    r = requests.post(api_url, data=json.dumps(json_text), headers=headers).json()
    code = r["errcode"]
    # print (code)
    if code == 0:
        log(time + ":消息发送成功 返回码:" + str(code) + "\n")
    else:
        log(time + ":消息发送失败 返回码:" + str(code) + "\n")
        exit(3)


if __name__ == '__main__':
    user = sys.argv[1]
    subject = sys.argv[2]
    text = info_format()

    send_msg(user, subject, text)
