#!/usr/bin/python3

import urllib.request
import json
import simplejson
import sys


def gettoken(corpid, corpsecret):
    gettoken_url = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=' + corpid + '&corpsecret=' + corpsecret
    print(gettoken_url)
    try:
        token_file = urllib.request.urlopen(gettoken_url)
    except urllib.request.HTTPError as e:
        print(e.code)
        print(e.read().decode("utf8"))
        sys.exit()
    token_data = token_file.read().decode('utf-8')
    token_json = json.loads(token_data)
    token_json.keys()
    token = token_json['access_token']
    return token


def senddata(access_token, user, subject, content):
    send_url = 'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=' + access_token
    send_values = {
        "touser": "JingMo", #企业号中的用户帐号，在zabbix用户Media中配置，如果配置不正常，将按部门发送
        "toparty": "3",
        "msgtype": "textcard",#消息类型
        "agentid": "1000006", #企业号中的应用id
        "textcard": {
            "title": "监控告警",
            "description": subject + '\n' + content,
            "url": "URL",
            "btntxt": "更多"
        },
        "safe": "0"
    }
    send_data = simplejson.dumps(send_values, ensure_ascii=False).encode('utf-8')
    #print(send_data)
    send_request = urllib.request.Request(send_url,send_data)
    #print(send_request)
    response = json.loads(urllib.request.urlopen(send_request).read())
    print(str(response))

if __name__ == '__main__':
    user = sys.argv[1]
    subject = sys.argv[2]
    content = sys.argv[3]
    corpid = 'ww7698c28eb86a9ae3'  #CorpID是企业号的标识
    corpsecret = 'ktGb49-y4SFXks3xGONj2JwUgwa3DXNPGVCaebAfbL8'  #corpsecretSecret是应用的Secret
    accesstoken = gettoken(corpid, corpsecret)
    senddata(accesstoken, user, subject, content)