
# 开源的云网盘
seafile




图形界面安装{
	# centos6.8 install
	yum groupinstall "Desktop" "X Window System" "Font"
	startx
		
	# centos 7.3 install
	yum groupinstall "GNOME Desktop" "Graphical Administration Tools"
}

vnc安装{
    yum install tigervnc-server
	cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@:1.service
    vim /etc/systemd/system/vncserver@:1.service
	systemctl enable vncservice@:1
	vncpassword
	systemctl start vncservice@:1
}

python 3.6.0 install for centos7.3{
	#不安装ncurses-devel会报：/usr/bin/ld: cannot find -lncurses
	yum -y install gcc zlib-devel openssl openssl-devel ncurses-devel rpm-build python-devel
	tar xf Python-3.6.0.tgz -C /usr/local/src/
	cd /usr/local/src/Python-3.6.0
	./configure --prefix=/usr/local/python-3.6.0
	make && make install
	echo "export PATH=/usr/local/python-3.6.0/bin:/\$PATH" > /etc/profile.d/python.sh
	. /etc/profile.d/python.sh
		
    #readline install,不安装的话，命令无法退格
	tar xf readline-6.2.4.1.tar.gz -C /usr/local/src/
	cd /usr/local/src/readline-6.2.4.1
	python3.6 setup.py install
		
    #pip install
	yum -y install python-pip  # yum install
	tar xf pip-9.0.1.tar.gz -C /usr/local/src/  # compile install
	cd /usr/lcoal/src/pip-9.0.1
	python3.6 setup.py install
	# pip command 
	    install
		uninstall
		freeze   # 以 requirements 格式列表输出当前已安装的程序包及其版本
		list    # 列表显示当前已安装的程序包
		show    # 列表显示当前已安装程序包的版本信息
		search    # 按指定关键字在 PyPI 上搜索程序包并显示结果列表
		wheel     # 根据指定 requirement 构建 wheel 文件
	pip install -h   # 查看子选项
	e.g1  
	pip --proxy=beproud:passwd@proxy.example.com:1234 install requests
	e.g2
	export PIP_PROXY=proxy.example.com:1234
	e.g3
	pip install flask bottle
	pip install ./logfilter-0.9.2     # 从源码包安装
	pip install -e ./logfilter-0.9.2   # 可编辑（editable）安装
	e.g4
	pip install hg+https://bitbucket.org/shimizukawa/logfilter  # clone install ,如果是git,需要把hg换成 git
	pip install -e hghg+https://bitbucket.org/shimizukawa/logfilter#egg=logfilter
	pip install -r requirements.txt   # 通过 requirements.txt 安装,也可以看出当前目录与该配置文件的差异
	pip install -U requests
	pip install --download-cache=~/.pip-cache requests
	export PIP_DOWNLOAD_CACHE=~/.pip-cache     # 在环境变量中设置下载缓存
	pip freeze > requirements.txt    # 将 pip freeze 的输出保存在 requirements.txt
	pip uninstall -y flask
	
	
	#ipython install（python3.0）
	tar xf ipython-6.1.0.tar.gz -C /usr/local/src/
	cd /usr/local/src/ipython-6.1.0
	python3.6 setup.py install
	# ipython depend1
	tar xf traitlets-4.3.2.tar.gz -C /usr/local/src/
	cd /usr/local/src/traitlets-4.3.2
	python3.6 setup.py install
	# ipython depend2
	tar xf six-1.10.0.tar.gz
	# ipython depend3
	tar xf ipython_genutils-0.2.0.tar.gz
}


python setup.py 命令{
    python setup.py bdist_wheel
	python setup.py bdist_wheel --universal
	python setup.py register
	python setup.py sdist bdist_wheel upload
	# alias用法
	python setup.py alias release register sdist bdist_wheel upload
	python setup.py release
	python setup.py check -r -s
}

 
pexpect(系统批量运维管理器){
	# ptyprocess-0.5.2 install, pexpect-4.2.1 depend
	tar xf ptyprocess-0.5.2.tar.gz
	cd ptyprocess-0.5.2
	python setup.py install
	
	# pexpect-4.2.1 install
	tar xf pexpect-4.2.1.tar.gz
	python setup.py install
}


wxPython安装{
	# install depend
	yum -y install gtk+ gtk+-devel zlib-devel libpng-devel libjpeg-devel 
	# install wxPython3.0.2.0
	tar xf wxPython-src-3.0.2.0.tar.bz2
	./configure --prefix=/usr/local/wxpython-3.0.2.0
	make && make install
}


paramiko安装{
	 
	# install paramiko
	pip install paramiko   or easy_install paramiko
	#e.g 1 使用密码连接，执行远程主机命令
	#!/usr/bin/env python
	import paramiko

	hostname='172.16.1.16'
	username='root'
	password='zang123..
	# send paramiko to syslogin.log
	paramiko.util.log_to_file('syslogin.log')

	ssh=paramiko.SSHClient()
	ssh.load_system_host_keys()

	ssh.connect(hostname=hostname,username=username,password=password)
	stdin,stdout,stderr=ssh.exec_command('mkdir /test/a/b/c -p')
	#print stdout.read()
	print stdout.read()
	ssh.close()
}		


env install
    # install python devel
	yum -y  install python-devel
	
		
centos install china yum{
    # backup
	mv /etc/yum.repos.d/CentOS-Bast.repo{,.bak}
    # centos 5 for base
    wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-5.repo		
	# centos 6	for base
	wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo	
	# centos 7 for base
	wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
	
	# backup
	mv /etc/yum.repos.d/epel.repo{,.bak}
	mv /etc/yum.repos.d/epel-testing.repo{,.bak}
	# centos 5 for epel
	wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-5.repo
    # centos 6 for epel
	wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-6.repo
    # centos 7 for epel
    wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo

    # pip yum
	vi ~/.pip/pip.conf
	[global]
	trusted-host = mirrors.aliyun.com
	index-url = http://mirrors.aliyun.com/pypi/simple

    pip2 install ipython==5.5
}


virtualenv install{
	yum -y install python-devel python-pip   #install python-devel
    pip install virtualenv    # show install version
	pip freeze  # virtualenv use
	mkdir ~/work
	cd ~/work
	virtualenv venv    # 生成环境目录
	source venv/bin/activate    # 启动 virtualenv环境
	pip freeze
	deactivate      # 关闭 virtualenv环境
	# 借助virtualenv 使用不同版本的python
	virtualenv --python=/usr/local/python3.6/bin/python venv2
	source venv2/bin/activate	
	# 在 不 activate的状态下执行 virtualenv 环境的命令
	venv/bin/python -c "import sys; print sys.executable"
	venv/bin/python
	import requests   # 从 venv环境中导入
	venv/bin/pip install flask   # 安装到 venv环境中
	# virtualenv 命令
	virtualenv --help
	    -p  --python   # 指定 virtualenv环境下使用的Python，格式： --python=/usr/local/bin/python2.7
		--system-stie-packages # 使用当前python主体上已经安装的程序库，默认忽略主体上已经安装的程序库
		--always-copy   # 无论符号链接是否可用，一概不使用符号链接，而是直接复制文件。即便是允许使用符号
		                # 链接的OS，在某些文件系统下仍会出现符号链接不可用的情况。此时就可以用到该选项
		--clear   # 删除指定的virtualenv环境中安装的依赖库，初始化环境。
		-q  -quiet    # 执行virtualenv命令时，控制向工作台输出的信息量。
		-v  -verbose   # 执行virtualenv命令时，增加向控制台输出的信息量。
	注：可以使用 $HOME/.virtualenv/virtualenv.ini 配置文件
	[virtualenv]
	python = /usr/local/bin/python2.7
	quiet = true
	system-site-packages = true
	注2：配置文件 -> 环境变量 -> 命令行传值
	e.g 环境变量配置
	--quiet       -> VIRTUALENV_QUIET=true
	--python      -> VIRTUALENV_PYTHON=/usr/local/bin/python2.7
	--system-site-packages   -> VIRTUALENV_SYSTEM_SITE_PACKAGES=true
	
	# flake8 install(编码格式、语法检测，可选)
	pip install flake8
	flake8 sample.py
}


python



python 调试器
    import pdb;pdb.set_trace()     # 在需要停止的python代码处插入此语句

	
web
	mkdir ~/work
	cd ~/work
	virtualenv venv    # 生成环境目录
	source venv/bin/activate    # 启动 virtualenv环境
    # install flask
	pip install -U Flask
	
	
 













































