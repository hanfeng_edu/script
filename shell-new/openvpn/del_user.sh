#!/usr/bin/env bash
# Author: jackzang
# mail: jackzang@aishangwei.net
# Date: 2020/2/9

set -e

USER_KEYS_DIR=/etc/openvpn/client/keys
EASY_RSA_VER=3
EASY_RSA_DIR=/etc/openvpn/easy-rsa/

for user in "$@"
do
  cd ${USER_KEYS_DIR}/${EASY_RSA_VER}
  echo -e 'yes\n' | ./easyrsa revoke ${user}
  ./easyrsa gen-crl
  if -d ${USER_KEYS_DIR}/${user} ; then
    rm -rf ${USER_KEYS_DIR}/${user}
  fi
done
systemctl restart openvpn@server
exit 0
