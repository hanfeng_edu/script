#!/usr/bin/env bash
# ******************************************************
# Filename     : saltstack_install.sh
# Last modified: 2017-07-20 18:36
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# master server install for centos7.3
yum install salt-master -y
systemctl start salt-master
systemctl enable salt-master


# client install for centos6.8
yum install salt-minion -y
service salt-minion start
chkconfig salt-minion on




































