#!/usr/bin/env bash
# ******************************************************
# Filename     :	
# Last modified:	2017-07-14 13:21
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos 6.8
# ******************************************************

ftp_download() {
       type lftp >/dev/null || yum -y install lftp
       file2="software/source/jdk/jdk-8u141-linux-x64.gz"
cat >/tmp/ftpcmd.txt<<EOF
open ftp.aishangwei.net:10024
user aishangweidownload aishangwei.net
ls 
mget $file1 
bye
EOF
       lftp -f /tmp/ftpcmd.txt
       rm -r /tmp/ftpcmd.txt
       mv ./$file1 ./$file2  /usr/local/src/
}

java_install(){
    tar xf /usr/local/src/jdk-8u141-linux-x64.gz -C /usr/local/
    ln -sv  /usr/local/jdk1.8.0_141 /usr/local/java
    echo "PATH=/usr/local/java/bin:\$PATH" > /etc/profile.d/java.sh
    . /etc/profile
}
#ftp_download
java_install







