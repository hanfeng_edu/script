#!/usr/bin/env bash
# ******************************************************
# Filename     :	fail2ban_install.sh
# Last modified:	2017-07-05 12:43
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

############ 请自行调整 #############################
# fail2banlog目录
fail2banlog=/data/logs/fail2ban
nginxlog=/data/www/logs/www.test.com_access.log

#####################################################

# install fail2ban
yum -y install fail2ban

# config fail2ban.conf
sed -i "s/logtarget = SYSLOG/logtarget = ${fail2banlog}\/fail2ban.log/g" /etc/fail2ban/fail2ban.conf

# config log
if [ ! -d ${fail2banlog} ];then
    mkdir -pv ${fail2banlog}
fi

##############DDOS防工具配置########################
cat >>/etc/fail2ban/jail.conf<<EOF
enabled = true
port = http,https
filter = nginx-bansniffer
action = iptables[name=IT300, port=http, protocol=tcp]
         sendmail-whois[name=IT300, dest=xxxxx@qq.com, sender=xxxxxx@163.com]
logpath = ${nginxlog}
maxretry = 300
findtime = 60
bantime = 3600
EOF

cat >/etc/fail2ban/filter.d/nginx-bansniffer.conf<<EOF
[Definition]

failregex = <HOST> -.*- .*HTTP/1.* .* .*$
ignoreregex =
EOF
###############DDOS END#############################

#
service fail2ban start
chkconfig --add fail2ban
chkconfig fail2ban on

# ddos模拟
#while true ; do wget http://www.xxx.com/404 ; done
#cat /data/logs/fail2ban.log
#iptables -L -n






