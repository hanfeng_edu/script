#!/usr/bin/env bash
# ******************************************************
# Filename     : tomcat_7.0.78_install.sh
# Last modified: 2017-06-30 09:17
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# php-fpm file
phpfpmF=/usr/local/php/etc/php-fpm.conf


# free Memory
freeM=$(free -m|grep Mem|awk '{print $4}')


#sed -i "s/^pm = dynamic/pm = static/g"   $phpfpmF
maxchildren=$(${freeM}/20)
sed -i "/^pm.max_children/ c pm.max_children = ${maxchildren}" $phpfpmF
sed -i "/^;pm.max_requests = 500/ a pm.max_requests = 5120" $phpfpmF
#sed -i "/^;request_terminate_timeout/ a request_terminate_timeout = 

























