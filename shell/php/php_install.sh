#!/usr/bin/env bash
# ******************************************************
# Filename     : php_install.sh
# Last modified: 2017-05-22 11:06
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************
PHPVER=php-5.5.38
PHPVER_POSTFIX=tar.gz

/usr/bin/ yum -y install gcc gcc-c++ autoconf automake zlib zlib-devel openssl openssl-devel pcre* make gd-devel libjpeg-devel libpng-devel libxml2-devel bzip2-devel libcurl-devel

/usr/bin/wget -P /tmp/ ftp://172.16.1.10/mis/software/source/lnmp/${PHPVEER}.${PHPVER_POSTFIX}
/usr/bin/cd /tmp/
/usr/bin/tar -xf ${PHPVEER}..${PHPVER_POSTFIX}
/usr/bin/cd /tmp/$PHPVER

./configure --prefix=/usr/local/$PHPVER --with-config-file-path=/usr/local/$PHPVER/etc --with-bz2 --with-curl --enable-ftp --enable-sockets --disable-ipv6 --with-gd --with-jpeg-dir=/usr/local --with-png-dir=/usr/local --with-freetype-dir=/usr/local --enable-gd-native-ttf --with-iconv-dir=/usr/local --enable-mbstring --enable-calendar --with-gettext --with-libxml-dir=/usr/local --with-zlib --with-pdo-mysql=mysqlnd --with-mysqli=mysqlnd --with-mysql=mysqlnd --enable-dom --enable-xml --enable-fpm --with-libdir=lib64 --enable-bcmath

make && make install

ln -s /usr/local/$PHPVER /usr/local/php
cp php.ini-production /usr/local/php/etc/php.ini
cp /usr/local/php/etc/{php.fpm.conf.default,php.fpm.conf}


