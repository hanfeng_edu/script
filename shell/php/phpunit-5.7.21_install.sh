#!/usr/bin/env bash
# ******************************************************
# Filename     :	phpunit-5.7.21_install.sh
# Last modified:	2017-06-21 14:50
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# 下载软件
ftp_upload() {
     type lftp >/dev/null || yum -y install lftp
     file1="software/source/php/$1"
cat >/tmp/ftpcmd.txt<<EOF
open ftp.aishangwei.net:10024
user aishangweidownload aishangwei.net
mget $file1
bye
EOF
      lftp -f /tmp/ftpcmd.txt
	  mv $1 /usr/local/src/
      rm -f /tmp/ftpcmd.txt
}


# 函数检测文件是否存在
checkfile_exist() {
      # 需要检测的文件名
      file[1]=phpunit-5.7.21.phar

      # 循环检测文件是否存在
      for i in ${file[@]}
      do
          if [ ! -e /usr/local/src/$i ];then
          echo -e "\033[41m --$i no exist!---\n ---Start download $i -- \033[0m"
          ftp_upload $i
          fi
      done

      # 如果执行结果为 1，就退出，不在执行后面命令
      #set -e
}

checkfile_exist



# install php5.6.30


# install phpunit-5.7.21
mv /usr/local/src/phpunit-5.7.21.phar /usr/local/bin/phpunit
phpunit -version







