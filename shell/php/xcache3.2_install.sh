#!/usr/bin/env bash
# ******************************************************
# Filename     : xcache3.2_install.sh
# Last modified: 2017-06-29 17:12
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# install xcache
tar xf /usr/local/src/xcache-3.2.0.tar.gz -C /usr/local/src/
cd /usr/local/src/xcache-3.2.0
/usr/local/php/bin/phpize --clean && /usr/local/php/bin/phpize
./configure --enable-xcache --with-php-config=/usr/local/php/bin/php-config
make && make install
cat /usr/local/src/xcache-3.2.0/xcache.ini >> /usr/local/php/etc/php.ini
service php-fpm restart

# monitor xcache
cp -a htdocs/ /data/www/www.test.com/xcache
cp /data/www/www.test.com/xcache/{config.default.php,config.php}

# cacher/mkpassword.php可以生成密码，默认使用 admin,密码：admin123..
sed -i '/xcache.admin.user/ c xcache.admin.user="admin"' /usr/local/php/etc/php.ini
sed -i '/xcache.admin.pass/ c xcache.admin.pass="413b483076832cb36e29c6a8de54ae1e"' /usr/local/php/etc/php.ini
echo "user:admin password:admin123.. http://domain/xcache"


