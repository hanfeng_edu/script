#!/usr/bin/env bash
# ******************************************************
# Filename     :	php5.6.0_install.sh
# Last modified:	2017-06-21 14:50
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : php5.6.30 for zabbix-server
# ******************************************************

PHP=php-5.6.30

# create user
NGINX=`sudo cat /etc/passwd |grep nginx`
if [ -z $NGINX ] ; then
   groupadd -r nginx
   useradd -r -g nginx -d /data/www -s /sbin/nologin nginx
else
   echo "nginx用户已创建，无需重新创建" >> ./install.log
fi


#wget -P /usr/local/src/ ftp://test1:test1\!@172.16.1.10/mis/software/source/php/${PHP}.tar.gz 

yum -y install gcc gcc-c++ openssl openssl-devel pcre pcre-devel libxml2-devel libcurl-devel libjpeg-devel libpng-devel libicu-devel openldap-devel freetype-devel libxml2

# install libmcrypt
tar -xf /usr/local/src/libmcrypt-2.5.8.tar.gz -C /usr/local/src/
cd libmcrypt-2.5.8
./configure --prefix=/usr/local
make && make install
echo "/usr/local/lib" > /etc/ld.so.conf.d/local.conf
ldconfig

# install php
tar xf /usr/local/src/${PHP}.tar.gz -C /usr/local/src/
cd /usr/local/src/$PHP
./configure \
   --prefix=/usr/local/$PHP\
   --enable-fpm \
   --enable-mysqlnd \
   --enable-opcache \
   --enable-pcntl \
   --enable-mbstring \
   --enable-soap \
   --enable-zip \
   --enable-calendar \
   --enable-bcmath \
   --enable-exif \
   --enable-ftp \
   --enable-intl \
   --enable-sockets \
   --enable-short-tags \
   --enable-static \
   --enable-xml \
   --enable-shmop \
   --enable-sysvsem \
   --enable-inline-optimization \
   --enable-mbregex \
   --enable-gd-native-ttf \
   --enable-pdo \
   --disable-rpath \
   --with-config-file-path=/usr/local/${PHP}/etc \
   --with-mysql=mysqlnd \
   --with-mysqli=mysqlnd \
   --with-pdo-mysql=mysqlnd \
   --with-fpm-user=nginx \
   --with-fpm-group=nginx \
   --with-libdir=lib64 \
   --with-openssl \
   --with-zlib \
   --with-curl \
   --with-gd \
   --with-zlib-dir=/usr/lib \
   --with-png-dir=/usr/lib \
   --with-jpeg-dir=/usr/lib \
   --with-freetype-dir=/usr/lib \
   --with-iconv-dir=/usr/local/libiconv \
   --with-libxml-dir \
   --with-gettext \
   --with-mhash \
   --with-mcrypt \
   --with-xsl \
   --with-xmlrpc \
   --with-ldap
make && make install

# php config
ln -sv /usr/local/$PHP /usr/local/php
cp /usr/local/php/etc/php-fpm.conf{.default,}
cp /usr/local/src/php-5.6.30/php.ini-production /usr/local/php/etc/php.ini

# php service
cp /usr/local/src/php-5.6.30/sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm
chmod +x /etc/init.d/php-fpm
chkconfig --add php-fpm
chkconfig php-fpm on
service php-fpm start

# test index.php
if [ ! -d /data/www/www.test.com ];then
       mkdir -pv /data/www/www.test.com
fi 
cat > /data/www/www.test.com/index.php <<EOF
<?php
php
?>
EOF




