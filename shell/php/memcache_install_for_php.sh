#!/usr/bin/env bash
# ******************************************************
# Filename     : memcache_install_for_php.sh
# Last modified: 2017-05-25 08:50
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************
tar -zxvf memcache-2.2.5.tgz
cd memcache-2.2.5
/usr/local/php/bin/phpize
./configure --enable-memcache --with-php-config=/usr/local/php/bin/php-config
make && make install
vi /usr/local/php/etc/php.ini
extension_dir="/usr/local/php/lib/php/extensions/no-debug-non-zts-20060613/"
extension=memcache.so
/usr/local/php/sbin/php-fpm -c /usr/local/php/etc/php/php.ini
