

# install kvm denpend
yum -y install libvirt-client gpxe-roms-qemu libvirt-python python-virtinst qemu-kvm virt-manager libvirt \
virt-viewer virt-top virt-what qemu-img

# install graph
yum groupinstall -y "Desktop" "Desktop Platform" "Desktop Platform Development" "Fonts" \
"General Purpose Desktop" "Graphical Administration Tools" "Graphics Creation Tools" "Input Methods" \
"X Window System" "Chinese Support[zh]" "Internet Browser"

yum groupinstall "GNOME Desktop" "Graphical Administration Tools"
sudo ln -sf /lib/systemd/system/runlevel5.target /etc/systemd/system/default.target

# install vnc, default view port 5901
yum -y install tigervnc tigervnc-server
echo 'VNCSERVERS="1:root"\nVNCSERVERARGS[2]="-geometry 800x600 -nolisten tcp -localhost"' >> /etc/sysconfig/vncservers
service vncserver restart


























