#!/usr/bin/env bash
# ******************************************************
# Filename     : mariadb_mha_mananger.sh
# Last modified: 2017-05-23 12:15
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************
masterha_check_repl --conf=/etc/masterha/app1.cnf
masterha_check_status --conf=/etc/masterha/app1.cnf

# start MHA manager,--remove_dead_master_conf:当发生主从切换后，老
# 主库IP会从配置文件移除。--ignore_last_failover,默认会在/data/目录下
# 产生app1.failover.complete文件，设置该参数后，即使不足8小时也可以再次切换。
nohup masterha_manager --conf=/etc/masterha/app1.cnf --remove_dead_master_conf --ignore_last_failover < /dev/null > /var/log/masterha/app1/manager.log 2>&1 &  
sleep 10
masterha_check_status --conf=/etc/masterha/app1.cnf

# stop MHA manager
# masterha_stop --conf=/etc/masterha/app1.cnf
