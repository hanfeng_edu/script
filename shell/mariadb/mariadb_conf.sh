#!/usr/bin/env bash
# ******************************************************
# Filename     : mariadb_yum_install.sh
# Last modified: 2017-05-22 20:52
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************
MYCNF_PATH=/etc/my.cnf
DATAPATH=/data/mariadb
chown -R  mysql:mysql $DATAPATH
sed -i "/datadir=/ c datadir=$DATAPATH" $MYCNF_PATH
sed -i "/datadir=/ a {skip_name_resolve=1 \ninnodb_file_per_table=1}" $MYCNF_PATH
