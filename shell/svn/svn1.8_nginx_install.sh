#!/usr/bin/env bash
# ******************************************************
# Filename     :	svn1.8_nginx_install.sh
# Last modified:	2017-07-18 13:27
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos6.8 ,subversion-1.8.18 需要 sqlite-3.7以上
# ******************************************************

# download software
ftp_download() {
       type lftp >/dev/null || yum -y install lftp
       file1="software/source/svn/subversion-1.8.18.tar.gz"
	   file2="software/source/sqlite/sqlite.tgz"
       #file3="shell/nginx/nginx-1.8.1_install.sh"
cat >/tmp/ftpcmd.txt<<EOF
open 139.196.96.164:10024
user aishangweidownload aishangwei.net
mget $file1 $file2
bye
EOF
       lftp -f /tmp/ftpcmd.txt
       rm -r /tmp/ftpcmd.txt
}

# install subversion
svn_install() {
       mv subversion-1.8.18.tar.gz sqlite.tgz /usr/local/src/
       yum -y install apr apr-devel

       # install sqlite-3.18.2(sql light database)
       tar xf sqlite.tgz -C /usr/local/src/
       cd /usr/local/src/sqlite
       ./configure --prefix=/usr/local/sqlite
       make && make install

       # install subversion
       cd /usr/local/src
       tar xf subversion-1.8.18.tar.gz -C /usr/local/src/
       ./configure --prefix=/usr/local/subversion-1.8.18 --with-sqlite=/usr/local/sqlite
	   make && make install
	   ln -sv /usr/local/{subversion-1.8.18,subversion}
}

# config subversion
svn_config() {
       # svn resource path
       svnrepo=/data/svn
	   # svn project name
	   svnproject=test.aishangwei.net
	   # subversion exec path
	   svnpath=/usr/local/subversion
	   mkdir -pv ${svnrepo}
       ${svnpath}/bin/svnadmin create /data/svn/${svnproject}
       
	   sed -i "/# anon-access/ a anon-access = none" ${svnrepo}/${svnproject}/conf/svnserve.conf
	   sed -i "/# auth-access/ a auth-access = write" ${svnrepo}/${svnproject}/conf/svnserve.conf
	   sed -i "/# password-db/ a password-db = passwd" ${svnrepo}/${svnproject}/conf/svnserve.conf
	   sed -i "/# authz-db/ a authz-db = authz" ${svnrepo}/${svnproject}/conf/svnserve.conf

cat >> ${svnrepo}/${svnproject}/conf/passwd <<EOF
admin = test123
test1 = test123
EOF

cat >> ${svnrepo}/${svnproject}/conf/authz <<EOF
[${svnproject}:/]
* = r
admin = rw
EOF

}

# config svn_server start 、iptables
svn_server() {
       svnrepo=/data/svn
	   svnpath=/usr/local/subversion
	   ${svnpath}/bin/svnserve -d -r ${svnrepo}
       echo "${svnpath}/bin/svnserve -d -r ${svnrepo}" >> /etc/rc.local
	   iptables -I INPUT -m state --state NEW -p tcp --dport 3690 -j ACCEPT
	  # iptables -I INPUT -m state --state NEW 222.92.129.130 -s -p tcp --dport 3690 -j ACCEPT
}

ftp_download
svn_install
svn_config
svn_server







