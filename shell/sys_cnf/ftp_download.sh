#!/usr/bin/env bash
# ******************************************************
# Filename     : test.sh
# Last modified: 2017-07-20 18:36
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

ftp_upload() {
       type lftp >/dev/null || yum -y install lftp
       file1="software/rpm/ansible/ansible-2.2.1.0-1.el7.noarch.rpm"
       file2="shell/redis/redis-3.0.7_install.sh"
cat >/tmp/ftpcmd.txt<<EOF
open ftp.aishangwei.net:10024
user aishangweidownload aishangwei.net
ls 
mget $file1 $file2
bye
EOF
       lftp -f /tmp/ftpcmd.txt
       rm -r /tmp/ftpcmd.txt
}

ftp_upload
