# Filename     :	init_sys.sh
# Last modified:	2017-06-21 13:12
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

wget  ftp://test1:test1\!@172.16.1.10/mis/shell/vim/+.vimrc -P /tmp 


cat /tmp/+.vimrc > /etc/vimrc

echo " * */2 * * * root /usr/sbin/ntpdate 172.16.1.10" >> /etc/crontab

sed -i 's/=enforcing/=disabled/g' /etc/selinux/config


SYSVERSION=`cat /etc/redhat-release |grep -o "release [67]"`

# disable iptables
  service iptables stop
  chkconfig iptables off
# system parameter optimization
echo "kernel.core_uses_pid = 1" > /etc/sysctl.conf
echo "kernel.msgmax = 65536" >> /etc/sysctl.conf
echo "kernel.msgmnb = 65536" >> /etc/sysctl.conf
echo "kernel.shmall = 4294967296" >> /etc/sysctl.conf
echo "kernel.shmmax = 68719476736" >> /etc/sysctl.conf
echo "kernel.sysrq = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.default.rp_filter = 1" >> /etc/sysctl.conf
echo "net.ipv4.ip_forward = 0" >> /etc/sysctl.conf
echo "net.ipv4.tcp_retrans_collapse = 0" >> /etc/sysctl.conf
echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_tw_buckets = 6000 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_sack = 1 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_window_scaling = 1 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_rmem = 4096 87380 4194304  " >> /etc/sysctl.conf
echo "net.ipv4.tcp_wmem = 4096 16384 4194304  " >> /etc/sysctl.conf
echo "net.core.wmem_default = 8388608 " >> /etc/sysctl.conf
echo "net.core.rmem_default = 8388608 " >> /etc/sysctl.conf
echo "net.core.rmem_max = 16777216 " >> /etc/sysctl.conf
echo "net.core.wmem_max = 16777216 " >> /etc/sysctl.conf
echo "net.core.netdev_max_backlog = 262144 " >> /etc/sysctl.conf
echo "net.core.somaxconn = 262144 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_orphans = 3276800 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_syn_backlog = 262144 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_timestamps = 0 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_synack_retries = 1 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_syn_retries = 1 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_tw_recycle = 1 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_tw_reuse = 1 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_mem = 94500000 915000000 927000000  " >> /etc/sysctl.conf
echo "net.ipv4.tcp_fin_timeout = 1 " >> /etc/sysctl.conf
echo "net.ipv4.tcp_keepalive_time = 1200 " >> /etc/sysctl.conf
echo "net.ipv4.ip_local_port_range = 1024 65535 " >> /etc/sysctl.conf
echo "* soft nofile 65535" >> /etc/security/limits.conf
echo "* hard nofile 65535" >> /etc/security/limits.conf
echo "* soft nproc 65535" >> /etc/security/limits.conf
echo "* hard nproc 65535" >> /etc/security/limits.conf


# yum source setting
if [ -d "/etc/yum.repos.d/bak" ];then  echo '' ;else mkdir /etc/yum.repos.d/bak;fi
mv /etc/yum.repos.d/{*.repo,bak}
cat >/etc/yum.repos.d/Centos6.8-Net.repo <<EOF
[centos6.8_cd_mirror]
name=CentOS6.8-cd-mirror
baseurl=ftp://172.16.1.10/isos/centos6.8
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
EOF
echo "end"

#reboot

