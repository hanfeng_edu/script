#!/usr/bin/env bash
# ******************************************************
# Filename     :	nginx_www.test.com_modifi.sh
# Last modified:	2017-07-11 15:52
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

SWWW=www.test.com
DWWW=fhdz.rshui.cn

# 修改虚拟机配置文件名称
mv /usr/local/nginx/conf.d/vhosts/{${SWWW}.conf,${DWWW}}.conf
# 修改虚拟机配置文件内容
sed -i "s/${SWWW}/${DWWW}/g" /usr/local/nginx/conf.d/vhosts/${DWWW}.conf

# 修改虚拟机数据文件
mv /data/www/${SWWW},${DWWW}

# 修改vsftp虚拟用户的家目录
sed -i "s/${SWWW}/${DWWW}/g" /etc/vsftpd/vsftpd_user_conf/rshuiftp1

# 重载nginx配置
service nginx reload




