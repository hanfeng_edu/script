#!/usr/bin/env bash
# ******************************************************
# Filename     : back_server_use.sh
# Last modified: 2017-08-03 16:18
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 凡是 ###开头的地方需要你根据实际情况配置
# ******************************************************

### mail
mail="zangxuefeng@rshui.cn"
mail2=""

# 日志临时存放目录
dailylogpath="/data/ftpsite"
dailylocallog="${dailylogpath}/daily_local_backup.log"
dailyremotelog="${dailylogpath}/daily_remote_backup.log"
dailylog="${dailylogpath}/daily.log"
[ ! -d $dailylogpath ] && mkdir -pv $dailylogpath 

dateb=`date +%Y%m%d`
# 检测备份是否成功
back_check(){
     ### 配置检测备份的目录
     backdir[0]="/data/ftpsite/101.251.236.67"
	 #backdir[1]="/data/backup/www/test2"
	 #backdir[2]="/data/backup/mysql/test1"
	 #backdir[3]="/data/backup/mysql/test2"
	 today=`date +%Y%m%d`
	
	 # 检测备份是否成功
     for i in ${backdir[@]}
     do
        backfile=`ls $i |grep -v log$|grep ${today}`
		ftpsitelog=`ls $i |grep log$|grep ${today}`
        if [ -n "$backfile" ] ;then
            for a in $backfile 
			do	      
			     echo "succeed--`date +%F-%R` backup $i/$a succeed." >> $dailylocallog
			done
		else
            echo "Failed---${i} backup fail!" >> $dailylocallog
	    fi
		
		if [ -n "$ftpsitelog" ] ;then
		    for b in $ftpsitelog
			do 
			    cat $i/$b >> $dailyremotelog
            done
        fi
     done

	 # 检测备份后目录文件大小变化
	 du -sh 
	 
	 #日志文件合并后，并清空日志文件
	 echo "--------  Local_log ----------" >$dailylog
     [ -e $dailylocallog ] && cat $dailylocallog >> $dailylog
	 succeedln="`grep -i ^succeed $dailylocallog |wc -l`"
	 failedln="`grep -i ^failed $dailylocallog |wc -l`"
	 echo "######本地检测备份文件，成功：$succeedln  失败：$failedln" >>$dailylog
	 
	 echo "--------  Remote_log ----------" >>$dailylog
     [ -e $dailyremotelog ] && cat $dailyremotelog >> $dailylog
	 succeedrn="`grep -i ^succeed $dailyremotelog |wc -l`"
	 failedrn="`grep -i ^failed $dailyremotelog |wc -l`"
	 echo "######远端检测备份文件，成功：$succeedrn  失败：$failedrn" >>$dailylog
	 
     echo " " >$dailylocallog
     echo " " >$dailyremotelog
}

# 发送邮件
mail_to() { 
     maillist1=$mail
	 maillist2=$mai2
     frommail=backserver
     mailx -s "系统备份日志" -r ${frommail}@back.cn  ${maillist1} < $dailylog
	 #mailx -s "系统备份日志" -r ${frommail}@back.cn  ${maillist2} < $dailylog
}

back_check
mail_to















