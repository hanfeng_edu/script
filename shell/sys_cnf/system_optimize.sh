# Filename     :	system_optimize.sh
# Last modified:	2017-06-29 09:10
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos6.8
# ******************************************************

###### 检查是否为64位系统，仅支持x64######
sys_judge() {

   # 检查是否root用户
   USER=`whoami`
   if [ $USER != "root" ];then
        echo "Please run this script as root !" >&2
        exit 1
    fi
   PLANTFORM=`uname -i`
   VERSION=`cat /etc/redhat-release |awk '{print $3}'`
   echo -e "\033[32m system  init script, Please Make sure that this your first install!!! press ctrl+C to cancel \033[0m"

    # 检查系统版本和架构 
    if [ $PLANTFORM != "x86_64" -o $VERSION != "6.8" ];then
        echo -e "\033[32m the script only Support CentOS_6.8 x86_64 \033[0m"
        exit 1
    else
        echo "The Platform is ok"
    fi

    # 询问是否继续
    read -n1 -p "Do you want to continue [Y/N]?" ANSWER

    if [ "$ANSWER" != "y" -a "$ANSWER" != "Y" ];then
         echo " Bye-----------Bye !"
         exit 0
    else
         echo -e "\033[32m Start to init System_Centos${VERSION}.......\033[0m"
    fi
}

###### 获取输入数据 ######
input_fun() {
     OUTPUT_VAR=$1
     INPUT_VAR=""
     while [ -z $INPUT_VAR];do
           read -p "$OUTPUT_VAR" INPUT_VAR
     done
     echo $INPUT_VAR
}


###### 获取网络参数 ######
input_again() {
     MYHOSTNAME=$(input_fun "please input the hostname(e.g test1):")
     DOMAINNAME=$(input_fun "please input the domainname(e.g aishangwei.net):")
     CARD_TYPE=$(input_fun "please input the card above you want to change(e.g eth0):")
     IPADDR=$(input_fun "please input ip address(e.g 192.168.100.1):")
     NETMASK=$(input_fun "please input netmask(e.g 255.255.255.0):")
     GATEWAY=$(input_fun "please input gateway(e.g 192.168.100.1):")
     MYDNS1=$(input_fun "please input DNS1(e.g 192.168.1.21):")
     MYDNS2=$(input_fun "please input DNS2(e.g 8.8.8.8):")
}


###### 设置网络参数 ######
network_change() {
    ifconfig -a
cat << EOF

+-------------------------------------------------+
|       查看当前网络IP信息，然后设置具体ip        |
+-------------------------------------------------+

EOF
input_again
MAC=$(ifconfig $CARD_TYPE | grep "HWaddr" | awk -F[" "]+ '{print $5}')

# 设置为固定IP模式
sed -i 's/BOOTPROTO=dhcp/BOOTPROTO=static/g' /etc/sysconfig/network-scripts/ifcfg-$CARD_TYPE 

# 设置固定IP
cat >>/etc/sysconfig/network-scripts/ifcfg-$CARD_TYPE <<ENDF
IPADDR=$IPADDR
NETMASK=$NETMASK
GATEWAY=$GATEWAY
DNS1=$MYDNS1
DNS2=$MYDNS2
ENDF

# 设置本地解析 host
cat >/etc/hosts <<ENDF
127.0.0.1 $MYHOSTNAME $MYHOSTNAME.$DOMAINNAME localhost
$IPADDR $MYHOSTNAME $MYHOSTNAME.$DOMAINNAME  localhost
ENDF

# 设置域名解析
cat >/etc/resolv.conf <<ENDF
domain $DOMAINNAME 
search $DOMAINNAME 
nameserver $MYDNS1 
nameserver $MYDNS2 
ENDF
}


###### 设置时间 ######
zone_time() {
     yum -y install ntp
     # 设置时区
     if [ `date +%z` != "+0800" ]; then
         echo 'ZONE="Asia/Shanghai"' > /etc/sysconfig/clock
         cp -f /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
     fi
     sleep 1
     # ntp时间更新
     /usr/sbin/ntpdate 0.centos.pool.ntp.org && /sbin/hwclock -w
cat >> /var/spool/cron/root << EOF
* * */1 * * /usr/sbin/ntpdate 0.centos.pool.ntp.org > /dev/null 2>&1
* * * * */1 /usr/sbin/hwclock -w > /dev/null 2>&1
EOF
     chmod 600 /var/spool/cron/root
     sleep 1
}


###### 内核和网络优化 ######
kernel_init() {

# 核心和网络参数调整
cat >/etc/sysctl.conf<<EMOF     
kernel.core_uses_pid = 1
kernel.msgmax = 65536
kernel.msgmnb = 65536
kernel.shmall = 4294967296
kernel.shmmax = 68719476736
kernel.sysrq = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.conf.default.rp_filter = 1
net.ipv4.ip_forward = 0
net.ipv4.tcp_retrans_collapse = 0
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_max_tw_buckets = 6000 
net.ipv4.tcp_sack = 1 
net.ipv4.tcp_window_scaling = 1 
net.ipv4.tcp_rmem = 4096 87380 4194304  
net.ipv4.tcp_wmem = 4096 16384 4194304  
net.core.wmem_default = 8388608 
net.core.rmem_default = 8388608 
net.core.rmem_max = 16777216 
net.core.wmem_max = 16777216 
net.core.netdev_max_backlog = 262144 
net.core.somaxconn = 262144 
net.ipv4.tcp_max_orphans = 3276800 
net.ipv4.tcp_max_syn_backlog = 262144 
net.ipv4.tcp_timestamps = 0 
net.ipv4.tcp_synack_retries = 1 
net.ipv4.tcp_syn_retries = 1 
net.ipv4.tcp_tw_recycle = 1 
net.ipv4.tcp_tw_reuse = 1 
net.ipv4.tcp_mem = 94500000 915000000 927000000  
net.ipv4.tcp_fin_timeout = 1 
net.ipv4.tcp_keepalive_time = 1200 
net.ipv4.ip_local_port_range = 1024 65535 
EMOF
}

####### 系统限制调整和优化 ######
system_init() {
     if [ ! -f "/etc/security/limits.conf.bak" ]; then
     cp /etc/security/limits.conf{,.bak}
     fi

     sed -i "/^*.*soft.*nofile/d" /etc/security/limits.conf
     sed -i "/^*.*hard.*nofile/d" /etc/security/limits.conf
     sed -i "/^*.*soft.*nproc/d" /etc/security/limits.conf
     sed -i "/^*.*hard.*nproc/d" /etc/security/limits.conf

cat >> /etc/security/limits.conf << EOF
#
#---------custom-----------------------
#
*           soft   nofile       65535
*           hard   nofile       65535
*           soft   nproc        65535
*           hard   nproc        65535
EOF

# 系统文件描述符调整
#sed -i "/^ulimit -SHn.*/d" /etc/rc.local
#echo "ulimit -SHn 102400" >> /etc/rc.local

#sed -i "/^ulimit -s.*/d" /etc/profile
#sed -i "/^ulimit -c.*/d" /etc/profile
#sed -i "/^ulimit -SHn.*/d" /etc/profile
#cat >> /etc/profile << EOF
#ulimit -c unlimited
#ulimit -s unlimited
#ulimit -SHn 102400
#EOF
}

###### 禁止 control+alt+delete 重启#######
set_key() {
      sed -i 's/^exec \/sbin\/shutdown/#exec \/sbin\/shutdown/' /etc/init/control-alt-delete.conf
      cat /etc/init/control-alt-delete.conf | grep /sbin/shutdown
      echo -e "\033[31m control-alt-delete ok \033[0m"
      sleep 1
}

####### 关闭selinux #######
selinux() {
      sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
      setenforce 0
      echo -e "\033[31m selinux ok \033[0m"
      sleep 1
}

###### ssh端口更改 ######
change_ssh_port() {
      # iptables allow ssh port
          iptables -I INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
          iptables -I INPUT  -m state --state NEW -s 222.92.129.130/32 -p tcp --dport 52113 -j ACCEPT
          iptables -I INPUT  -m state --state NEW -s 58.240.192.246/32 -p tcp --dport 52113 -j ACCEPT
          iptables -I INPUT  -m state --state NEW -s 103.244.234.150/32 -p tcp --dport 52113 -j ACCEPT
          service iptables save
          service iptables reload
      # motification ssh port
          #sed -i "/#Port 22/ c Port 22 \nPort 52113" /etc/ssh/sshd_config 
          sed -i "/#Port 22/ c Port 52113" /etc/ssh/sshd_config 
          service sshd reload
}      

check_exec(){
      echo "test"       
}

#检查是否为64位系统，仅支持centos6.8_x64
sys_judge

# 获取网络参数
#input_again

# 设置网络参数 
#network_change

# 设置时间
zone_time

# 内核和网络优化
kernel_init

# 系统限制调整和优化
system_init

# 禁止 control+alt+delete 重启
set_key

# 关闭selinux 
selinux

# ssh端口更改
change_ssh_port
      
# 检查执行结果
check_exec









