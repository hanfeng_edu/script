#!/bin/bash

set +x
if ! [ -f '/tmp/times.txt' ];then
	echo 'file not found!'
	exit 1	
fi

if [ "$(cat /tmp/times.txt | wc -l)" -ge 2 ];then
	sed -i '/tune/d' /etc/rc.local
	sed -i '/times/d' /etc/rc.local
	rm /tmp/times.txt
fi

eth0=`ip addr show eth0 2> /dev/null`
if ! [ "$eth0" ];then
	echo 'configured yet!'
	exit 1
fi

mac=`ip address show dev eth0 | grep "\([0-9|A-Z|a-z]\{2\}:\)\{5\}[0-9|A-Z|a-z]\{2\}" | awk '{print $2}'`
sed '/eth0/s/^/#/' -i /etc/udev/rules.d/70-persistent-net.rules
sed 's/eth0/eth0/' -i /etc/udev/rules.d/70-persistent-net.rules

sed "s/\(HWADDR=\).*/\1$mac/" -i /etc/sysconfig/network-scripts/ifcfg-eth0
sed '/UUID/d' -i /etc/sysconfig/network-scripts/ifcfg-eth0
ip=`cat /root/ip.txt`
if [ "$ip" = '' ];then
	exit 1
fi
echo "172.16.1.$ip"
sed "s/\(IPADDR=172.16.1.\).*/\1$ip/" -i /etc/sysconfig/network-scripts/ifcfg-eth0
sed "s/\(HOSTNAME=\).*/\1centos$ip/" -i /etc/sysconfig/network

echo 'nameserver 223.6.6.6' >> /etc/resolv.conf

echo '0 */12 * * * /usr/sbin/ntpdate cn.ntp.org.cn >/dev/null 2 >&1'>> /var/spool/cron/root

/etc/init.d/iptables start 
iptables -F
/etc/init.d/iptables save

chkconfig blk-availability off
chkconfig ip6tables off
chkconfig postfix off
chkconfig mdmonitor off

chattr +i /etc/passwd
chattr +i /etc/inittab
chattr +i /etc/group
chattr +i /etc/shadow
chattr +i /etc/gshadow

echo '*  -  nofile  1048576' >> /etc/security/limits.conf
echo deadline > /sys/block/vda/queue/scheduler
echo 'echo deadline > /sys/block/vda/queue/scheduler' >> /etc/rc.local

cp /etc/sysctl.conf /etc/sysctl.conf_`date +%F`
 
echo -e "net.core.somaxconn = 262144" >> /etc/sysctl.conf
echo -e "net.core.netdev_max_backlog = 262144" >> /etc/sysctl.conf
echo -e "net.core.wmem_default = 8388608" >> /etc/sysctl.conf
echo -e "net.core.rmem_default = 8388608" >> /etc/sysctl.conf
echo -e "net.core.rmem_max = 16777216" >> /etc/sysctl.conf
echo -e "net.core.wmem_max = 16777216" >> /etc/sysctl.conf
echo -e "net.ipv4.route.gc_timeout = 20" >> /etc/sysctl.conf
echo -e "net.ipv4.ip_local_port_range = 1024 65535" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_retries2 = 5" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_fin_timeout = 30" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_syn_retries = 1" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_synack_retries = 1" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_timestamps = 0" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_tw_recycle = 1" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_tw_reuse = 1" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_keepalive_time = 120" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_keepalive_probes = 3" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_keepalive_intvl = 15" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_max_tw_buckets = 36000" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_max_orphans = 3276800" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_max_syn_backlog = 262144" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_wmem = 8192 131072 16777216" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_rmem = 32768 131072 16777216" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_mem = 94500000 915000000 927000000" >> /etc/sysctl.conf
echo -e "net.ipv4.tcp_slow_start_after_idle = 0" >> /etc/sysctl.conf
echo -e "vm.swappiness = 5" >> /etc/sysctl.conf
echo -e "kernel.panic = 5" >> /etc/sysctl.conf
echo -e "kernel.panic_on_oops = 1" >> /etc/sysctl.conf  
echo -e "kernel.core_pipe_limit = 0" >> /etc/sysctl.conf
#echo -e "vm.panic_on_oom = 1" >> /etc/sysctl.conf
#echo -e "kernel.panic = 10" >> /etc/sysctl.conf
#iptables
echo -e "net.nf_conntrack_max = 25000000" >> /etc/sysctl.conf
echo -e "net.netfilter.nf_conntrack_max = 25000000" >> /etc/sysctl.conf
echo -e "net.netfilter.nf_conntrack_tcp_timeout_established = 180" >> /etc/sysctl.conf
echo -e "net.netfilter.nf_conntrack_tcp_timeout_time_wait = 120" >> /etc/sysctl.conf
echo -e "net.netfilter.nf_conntrack_tcp_timeout_close_wait = 60" >> /etc/sysctl.conf
echo -e "net.netfilter.nf_conntrack_tcp_timeout_fin_wait = 120" >> /etc/sysctl.conf

if [ -n "$eth0" ] && [ "$(cat /tmp/times.txt | wc -l)"  -eq 1 ];then
	shutdown -r now
fi