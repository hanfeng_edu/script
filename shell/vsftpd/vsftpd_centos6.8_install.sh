#!/usr/bin/env bash
# ******************************************************
# Filename     : vsftpd_centos6.8_install.sh
# Last modified: 2017-06-28 13:06
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for rshui centos6.8  
# ******************************************************

# set iptables source IP
SIP=222.92.129.130
SIP2=58.240.192.246
SIP3=103.244.234.150

# create dir
FTPHOME=/data/www/www.test.com
mkdir -pv $FTPHOME

# install vsftpd
VSFTPD=`sudo rpm -qa |grep vsftpd`
if [ -z $VSFTPD ] ; then
   yum install -y vsftpd
else
   echo "vsftpd 已安装，无需再安装" >> ./install.log
fi

# create user
NGINX=`sudo cat /etc/passwd |grep nginx`
if [ -z $NGINX ] ; then
   group -r nginx
   useradd -r -g nginx -d /data/www -s /sbin/nologin nginx
else
   echo "nginx用户已创建，无需重新创建" >> ./install.log
fi
chown -R nginx:nginx /data/www
chmod -R 755 /data/www

# install software
yum -y install db4-utils

# ftp user
cat >/etc/vsftpd/login.txt <<EOF
rshuiftp1
R)kj)36DnJ
rshuiftp2
R)kj)67DnJ
rshuiftp3
R)kj)67Dlo
rshuiftp4
R)ii)67Dlo
rshuiftp5
R)ii)67klo
EOF
chmod 600 /etc/vsftpd/login.txt

# create virual_user DB
db_load -T -t hash -f /etc/vsftpd/login.txt /etc/vsftpd/vsftpd_login.db
chmod 600 /etc/vsftpd/vsftpd_login.db

# modification vsftpd.conf
sed -i "/anonymous_enable/ c anonymous_enable=NO" /etc/vsftpd/vsftpd.conf
sed -i "/#chown_uploads/ c chown_uploads=NO" /etc/vsftpd/vsftpd.conf
sed -i "/#anon_upload_enable/ c anon_upload_enable=NO" /etc/vsftpd/vsftpd.conf
sed -i "/#async_abor_enable=YES/ c async_abor_enable=YES" /etc/vsftpd/vsftpd.conf
sed -i "/#ascii_upload_enable=YES/ c ascii_upload_enable=YES" /etc/vsftpd/vsftpd.conf
sed -i "/#ascii_download_enable=YES/ c ascii_download_enable=YES" /etc/vsftpd/vsftpd.conf
cat >>/etc/vsftpd/vsftpd.conf<<EOF
use_localtime=YES

listen_port=10024
chroot_local_user=YES
idle_session_timeout=300
data_connection_timeout=1

guest_enable=YES
guest_username=nginx
user_config_dir=/etc/vsftpd/vsftpd_user_conf
virtual_use_local_privs=YES

pasv_min_port=10060
pasv_max_port=10090

accept_timeout=5
connect_timeout=1
EOF

# modification authentication file
cat >/etc/pam.d/vsftpd<<EOF
#%PAM-1.0
auth sufficient /lib64/security/pam_userdb.so db=/etc/vsftpd/vsftpd_login
account sufficient /lib64/security/pam_userdb.so db=/etc/vsftpd/vsftpd_login
EOF

mkdir -pv /etc/vsftpd/vsftpd_user_conf

# create user privilege
for k in $( seq 1 5 )
do
cat >/etc/vsftpd/vsftpd_user_conf/rshuiftp${k}<<EOF
local_root=$FTPHOME
write_enable=YES
#file_open_mode=0777
###the file right after uploading 
anon_world_readable_only=NO
anon_upload_enable=YES
anon_mkdir_write_enable=YES
anon_other_write_enable=YES
EOF
done

# open iptables
iptables -I INPUT -s $SIP    -p tcp --dport 10024 -j ACCEPT
iptables -I INPUT -s $SIP2   -p tcp --dport 10024 -j ACCEPT
iptables -I INPUT -s $SIP3   -p tcp --dport 10024 -j ACCEPT
iptables -I INPUT -s $SIP    -p tcp --dport 10060:10090 -j ACCEPT
iptables -I INPUT -s $SIP2   -p tcp --dport 10060:10090 -j ACCEPT
iptables -I INPUT -s $SIP3   -p tcp --dport 10060:10090 -j ACCEPT
service iptables save
service iptables reload

# selinux setting
sed -i "/SELINUX=/ c SELINUX=disabled" /etc/selinux/config
setenforce 0

# start service
service vsftpd start
chkconfig --add vsftpd
chkconfig vsftpd on







