#!/usr/bin/env bash
# ******************************************************
# Filename     : openvpn-2.4.3_install.sh
# Last modified: 2017-08-07 09:18
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos 6.8
# ******************************************************

openvpnv="openvpn-2.3.17"
easyrsav="easy-rsa-2x"

### config ca
# config KEY_COUNTRY 
kcountry="CN"
# config KEY_PROVINCE
kprovince="HeNan"
kcity="ZMD"
korg="aishangwei.net"
kemail="jack.zang@aishangwei.net"
kcn=""
kname=""
kou="IT"



# download software
ftp_download() {
     type lftp >/dev/null 2>/dev/null || yum -y install lftp
     file1="software/source/openvpn/$1"
cat >/tmp/ftpcmd.txt<<EOF
open ftp.aishangwei.net:10024
user aishangweidownload aishangwei.net
ls 
mget $file1
bye
EOF
      lftp -f /tmp/ftpcmd.txt
	  mv $1 /usr/local/src/
      rm -f /tmp/ftpcmd.txt
}

# check software exists
checkfile_exist() {
      # check file name
      file[1]=${openvpnv}.tar.gz
      file[2]=${easyrsav}.zip


      
      for i in ${file[@]}
      do
          if [ ! -e /usr/local/src/$i ];then
          echo -e "\033[41m --$i no exist!-- \033[0m"
          echo -e "\033[32m --$i start downloading -- \033[0m"
		  ftp_download $i
		  fi
      done

      # 如果执行结果为 1，就退出，不在执行后面命令
      set -e
}

checkfile_exist

# install openvpn depend package
yum -y install pam pam-devel lzo lzo-devel

# install openvpn
tar xf /usr/local/src/${openvpnv}.tar.gz -C /usr/local/src/
cd /usr/local/src/$openvpnv
./configure --prefix=/usr/local/$openvpnv
make && make install
ln -sv /usr/local/{${openvpn},openvpn}
# install easy-rsa
unzip -p /usr/local/src/${easyrsav}.zip
cp -r /usr/local/src/$easyrsav/2.0 /usr/local/openvpn/$easyrsav
ln -r /usr/local/openvpn/{${easyrsav},easy-rsa}
# config easy-rsa
easyvar="/usr/local/openvpn/easy-rsa/vars"
sed -i /KEY_COUNTRY/ c KEY_COUNTRY="${kcountry}" $easyvar
sed -i /KEY_PROVINCE/ c KEY_PROVINCE="${kprovince}" $easyvar
sed -i /KEY_CITY/ c KEY_CITY="${kcity}" $easyvar
sed -i /KEY_ORG/ c KEY_ORG="${korg}" $easyvar
sed -i /KEY_EMAIL/ c KEY_EMAIL="${kemail}" $easyvar
sed -i /KEY_CN/ c KEY_CN="${kcn}" $easyvar
sed -i /KEY_NAME/ c KEY_NAME="${kname}" $easyvar
sed -i /KEY_OU/ c KEY_OU="${kou}" $easyvar
sed -i s/PKCS11_MODULE_PATH/#export PKCS11_MODULE_PATH=changeme/g $easyvar
sed -i s/PKCS11_PIN/#export PKCS11_PIN=1234/g $easyvar

未完，参考有道云笔记


