#!/usr/bin/env bash
# ******************************************************
# Filename     : python_for_ubuntu.sh
# Last modified: 2017-08-28 13:06
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for ubuntu
# ******************************************************

install python{  
	
	# deb package update、upgrade
	sudo apt-get -y update
	sudo apt-get -y upgrade
	sudo apt-get -y install build-essential libsqlite3-dev libreadline6-dev libgdm-dev \
                            zliblg-dev libbz2-dev sqlite3 tk-dev zip libssl-dev	
   
    # install python-dev
	sudo apt-get -y install python-dev
	# install pip
	wget https://bootstrap.pypa.io/get-pip.py
	sudo python get-pip.py
    # upgrade for pip
	python -m ensurepip -U	
	
    python -V
	pip --version
    pip freeze      # view the software under the /usr/local/lib/python2.7

	
    # install virtualenv
	sudo pip install virtualenv
	virtualenv --version
	mkdir ~/work
	cd ~/work
	virtualenv venv      # generate venv environment dir
	source venv/bin/activate      # start virtualenv environment
	deactivate      # close virtualenv
	
	virtualenv --python=/opt/python2.7.9/bin/python venv2   #generate venv2
	source venv2/bin/activate
	
}

install mercurial{
	
	# install Mercurial
	sudo pip install mercurial
	
	hg --version
	
	
	
	
	
	
	
	
	
	
	
	
	
	}















