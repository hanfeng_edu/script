#!/usr/bin/env bash
# ******************************************************
# Filename     : python-3.6.0_install.sh
# Last modified: 2017-06-28 13:06
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos 6.8
# ******************************************************

# 不安装该软件，命令无法正常输入
yum -y install readline-devel
tar xf python-3.6.0.tar.gz -C /usr/local/src/
./configure --prefix=/usr/local/python-3.6.0
make && make install

















