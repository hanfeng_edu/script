#!/usr/bin/env bash
# ******************************************************
# Filename     : ipython-6.1.0_install.sh
# Last modified: 2017-06-28 13:06
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos 6.8
# ******************************************************

# 不安装该软件，命令无法正常输入
yum -y install readline-devel

tar xf ipython-6.1.0 -C /usr/local/src
cd /usr/local/src/ipython-6.1.0
# 有可能网速问题，如果一次不成功，多试一次
/usr/local/Python-3.6.0/bin/pip3  install .
/usr/local/Python-3.6.0/bin/ipython3















