#!/usr/bin/env bash
# ******************************************************
# Filename     : tengine_install.sh
# Last modified: 2017-05-24 12:25
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : tengine 2.1.1
# ******************************************************


vim /usr/local/tengine/conf/nginc.conf
location ~ .php$ {
        root           html;
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi_params;
}
