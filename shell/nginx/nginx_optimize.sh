#!/usr/bin/env bash
# ******************************************************
# Filename     : nginx_optimize.sh
# Last modified: 2017-05-25 22:13
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# 由于高并发影响写操作，关闭网站文件的访问时间
vim /etc/fstab
/dev/sdb1   /data/www    defaults,noatime,nodiratime 0 0

# 调整单用户最大文件数和最大进程数
vim /etc/security/limits.conf
...
* soft nofile 65535
* hard nofile 65535
* soft nproc 65535
* soft nproc 65535
ulimit -n
ulimit -u
ulimit -a

# 优化内核TCP
# set timeout ,default 180000
net.ipv4.tcp_max_tw_buckets = 6000
# set system open port,default 32768-61000
net.ipv4.ip_local_port_range = 1024 65000
# set whether on timewait 快速回收，default 0
net.ipv4.tcp_tw_recycle = 1
# 是否开启重新使用，即允许将 time-wait sockets 重新用于新的TCP连接
# default 0
net.ipv4.tcp_tw_reuse = 1
# 如果启用syn cookies,当出现syn等待队列溢出时，使用cookies处理
# default 0
net.ipv4.tcp.syncookies = 1
# web应用中listen函数的backlog默认会将内核参数的 net.core.somaxconn 限制
# 到128，而Nginx定义的ngx_listen_backlog，默认为511，所以有必要调整这个值
# default 32768
net.core.somaxconn = 262144
# 设置被输送到队列数据包的最大数目，在网卡接受数据包的速率比内核处理数据
# 包的速率快时，会出现排队现象，该参数就是用于设置该队列的大小
# default 32768
net.core.netdev_max_backlog = 2622144
# 该参数用于设置linux能够处理不属于任何进程的套接字数量，所谓不属于任何进
# 程就是“孤儿”（orphan）进程，在快速、大量的连接中这种进程会很多，因此
# 要适当地设置该参数，如果这种“孤儿”进程套接字数量大于这个指定的值，那么
# 在使用dmesg查看时会出现“too many of orphaned sockets”警告。default 32768
net.ipv4.tcp_max_orphans = 262144
# 该参数用于记录尚未收到客户端确认信息的连接请求的最大值。defalut 1024
net.ipv4.tcp_max_syn_backlog = 262144
# 该参数用于设置使用时间戳作为序列号，通过这样的设置可以避免序列号被重复使用
# 在高速，高并发的环境中，这种情况是存在的，因此通过时间戳能够让这些被看做是
# 异常的数据包被内核接收，0表示关闭，default 1
net.ipv4.tcp_timestamps = 0
# 设置 syn ack重试的次数。减少该参数有利于避免DDOS攻击，default 5
net.ipv4.tcp_synack_retries = 1
# 设置在内核放弃建立连接之前发送 SYN包的数量，default 5
net.ipv4.tcp_syn_retries = 1
# 表示如果套接字由本端关闭，这个参数决定了它保持在FIN-WAIT-2状态的时间。default 60
# 当启用keepalive的时候，用于设置 TCP发送keepalive消息的频度
net.ipv4.tcp_fin_timeout = 1
net.ipv4.tcp_keepalive_time = 30


# 优化 Nginx
# 关闭访问日志，只开启必要的日志
# 使用 epoll
# nginx 配置优化
worker_connections 65535
keepalive_timeout 60
client_header_buffer_size 8k
worker_rlimit_nofile 65535




















