#!/usr/bin/env bash
# ******************************************************
# Filename     : nginx_install.sh
# Last modified: 2017-07-27 09:24
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos6.8
# ******************************************************

# nginx version
NGINX=nginx-1.12.2
# nginx install path
NIP=/usr/local

# 下载软件
ftp_upload() {
     type lftp >/dev/null || yum -y install lftp
     file1="software/source/nginx/$1"
cat >/tmp/ftpcmd.txt<<EOF
open ftp.aishangwei.net:10024
user aishangweidownload aishangwei.net
mget $file1
bye
EOF
      lftp -f /tmp/ftpcmd.txt
	  mv $1 /usr/local/src/
      rm -f /tmp/ftpcmd.txt
}


# 函数检测文件是否存在
checkfile_exist() {
      # 需要检测的文件名
      file[1]=gperftools-2.5.93.tar.gz
      file[2]=libunwind-1.2.tar.gz
      file[3]=nginx-1.12.2.conf
      file[4]=nginx-1.12.2.server
      file[5]=nginx-1.12.2.tar.gz
      file[6]=nginx-1.12.2.vhost_www.test.com
     #file[7]=ngx_pagespeed-1.8.31.2-beta.zip
     #file[8]=1.8.31.2.tar.gz

      # 循环检测文件是否存在
      for i in ${file[@]}
      do
          if [ ! -e /usr/local/src/$i ];then
          echo -e "\033[41m --$i no exist! start downling----- \033[0m"
          ftp_upload $i
		  fi
      done

      # 如果执行结果为 1，就退出，不在执行后面命令
      set -e
}

checkfile_exist

# install compiling envi
yum -y install gcc gcc-c++ make libtool zlib zlib-devel openssl openssl-devel pcre pcre-devel

# install ngx_pagespeed
#unzip /usr/local/src/ngx_pagespeed-1.8.31.2-beta.zip -d $NIP
#tar -xf /usr/local/src/1.8.31.2.tar.gz -C /usr/local/ngx_pagespeed-1.8.31.2-beta/
#ln -sv /usr/local/ngx_pagespeed-1.8.31.2-beta /usr/local/ngx_pagespeed

# install libunwind
tar xf /usr/local/src/libunwind-1.2.tar.gz -C /usr/local/
cd /usr/local/libunwind-1.2
./configure
make && make install

# install gperftools
tar xf /usr/local/src/gperftools-2.5.93.tar.gz -C /usr/local/
cd /usr/local/gperftools-2.5.93
./configure
make && make install

# reload /usr/local/lib 
echo "/usr/local/lib" > /etc/ld.so.conf.d/usr_local_lib.conf
/sbin/ldconfig

# create user
groupadd -r nginx
useradd -r -g nginx -s /sbin/nologin -d /data/www nginx

# nginx install
tar xf /usr/local/src/${NGINX}.tar.gz -C /usr/local/src/
cd /usr/local/src/$NGINX
./configure \
  --prefix=/usr/local/${NGINX} \
  --user=nginx \
  --group=nginx \
  --with-http_ssl_module \
  --with-http_realip_module \
  --with-http_addition_module \
  --with-http_sub_module \
  --with-http_dav_module \
  --with-http_flv_module \
  --with-http_mp4_module \
  --with-http_stub_status_module \
  --with-http_gunzip_module \
  --with-http_gzip_static_module \
  --with-http_random_index_module \
  --with-http_secure_link_module \
  --with-http_auth_request_module \
  --with-pcre \
  --with-mail \
  --with-mail_ssl_module \
  --with-file-aio \
  --http-client-body-temp-path=/var/tmp/nginx/client \
  --http-proxy-temp-path=/var/tmp/nginx/proxy \
  --http-fastcgi-temp-path=/var/tmp/nginx/fastcgi \
  --http-uwsgi-temp-path=/var/tmp/nginx/uwsgi \
  --http-scgi-temp-path=/var/tmp/nginx/scgi \
  --with-google_perftools_module 
  #--add-module=/usr/local/ngx_pagespeed

make && make install
mkdir -pv /var/tmp/nginx/{client,proxy,fastcgi}
ln -sv /usr/local/$NGINX /usr/local/nginx

# nginx.conf setting
cat /usr/local/src/nginx-1.12.2.conf > /usr/local/nginx/conf/nginx.conf
mkdir /usr/local/nginx/conf/vhosts

# vhosts/www.test.com
cat /usr/local/src/nginx-1.12.2.vhost_www.test.com > /usr/local/nginx/conf/vhosts/www.test.com.conf

# nginx service
mv /usr/local/src/nginx-1.12.2.server /etc/init.d/nginx
chmod +x /etc/init.d/nginx
chkconfig --add nginx
chkconfig nginx on

# firewall setting
firewall-cmd --permanent --zone=public --add-port=80/tcp
firewall-cmd --reload

# selinux setting
sed -i "/SELINUX=/ c SELINUX=disabled" /etc/selinux/config
setenforce 0

# create www dir
if [ ! -d /data/www/www.test.com ] ; then
   mkdir -pv /data/www/www.test.com
   mkdir -pv /data/www/logs/www.test.com
   chown -R nginx:nginx /data/www
   chmod -R 755 /data/www
else
   echo "目录已存在。" >> ./install.log
fi

# 日志分割
cat>/etc/logrotate.d/nginx<<EOF
/data/www/logs/*.log {
daily
rotate 10
sharedscripts
postrotate
    if [ -f /usr/local/nginx/logs/nginx.pid ]; then
        kill -USR1 `cat /usr/local/nginx/logs/nginx.pid`
    fi
endscript
}
EOF

# create index.html
echo "this is test.html" >/data/www/www.test.com/index.html

service nginx start

