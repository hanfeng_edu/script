#!/bin/bash
VIP=172.16.1.101
RIP1=172.16.1.42
RIP2=172.16.1.41
VIPPORT=3306
RIP1PORT=3306
RIP2PORT=3306
RIP1W=1
RIP2W=1
# weigh = {rr,wrr,lc,wlc,lblc,lblcr,dh,sh}
WEIGH=wrr

    case "$1" in
       start)
          echo "start LVS of DirectorServer"
          echo 1 > /proc/sys/net/ipv4/ip_forward
         #Set the Virtual IP Address
         /sbin/ifconfig lo:0 $VIP broadcast $VIP netmask 255.255.255.255 up
         /sbin/route add -host $VIP dev lo:0
         #Clear IPVS Table
         /sbin/ipvsadm -C
         #Set Lvs
         /sbin/ipvsadm -A -t $VIP:$VIPPORT -s wrr
         /sbin/ipvsadm -a -t $VIP:$VIPPORT -r $RIP1:$RIP1PORT -g -w${RIP1W}
       #  /sbin/ipvsadm -a -t $VIP:$VIPPORT -r $RIP2:$RIP2PORT -g -w${RIP2W}
         #Run Lvs
         /sbin/ipvsadm
         ;;
       stop)
         echo "close LVS Directorserver"
         /sbin/ipvsadm -C
         /sbin/ifconfig lo:0 down
         echo 0 > /proc/sys/net/ipv4/ip_forward
         ;;
        *)
         echo "Usage： $0 {start|stop}"
         exit 1
     esac
