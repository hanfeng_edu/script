#!/usr/bin/env bash
# ******************************************************
# Filename     :	elasticsearch1.7.2_install.sh
# Last modified:	2017-07-25 10:36
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

ftp_upload() {
    type lftp >/dev/null || yum -y install lftp
    file1="software/source/elk/elasticsearch-1.7.2.tar.gz"
	file2="software/source/jdk/jdk-8u141-linux-x64.gz"
cat >/tmp/ftpcmd.txt<<EOF
open ftp.aishangwei.net:10024
user aishangweidownload aishangwei.net
ls 
mget $file1 $file2
bye
EOF
    lftp -f /tmp/ftpcmd.txt
    rm -r /tmp/ftpcmd.txt
	mv ./$file1 ./$file2  /usr/local/src/
}

java_install(){
    tar xf /usr/local/src/jdk-8u141-linux-x64.gz -C /usr/local/
    ln -sv  /usr/local/jdk1.8.0_141 /usr/local/java
    echo "PATH=/usr/local/java/bin:\$PATH" > /etc/profile.d/java.sh
    . /etc/profile
}


# node1 安装，node2和node1基本一致，就是 node.name 不一致而已
elasticsearch_cluster_install(){
    elastv=elasticsearch-1.7.2
	host=172.16.1.12
	# elasticsearch data path
	pdata=/data/elasticsearch/es-data
	# elasticsearch worker path
	pworker=/data/elasticsearch/es-worker
	[ ! -d $pdata ] && mkdir -p $pdata
	[ ! -d $pworker ] && mkdir -p $pworker
	
	# install elasticsearch
	tar xf /usr/local/src/${elastv}.tar.gz -C /usr/local
	ln -sv /usr/local/${elastv} /usr/local/elasticsearch > /dev/null
	
	###### elasticsearch.yml config
cat >> /usr/local/elasticsearch/config/elasticsearch.yml<<EOF
cluster.name: elasticsearch 
node.name: $1
node.master: true
node.data: true
path.data: ${pdata}
path.work: ${pworker}
#discovery.zen.ping.multicast.enabled: false
#discovery.zen.ping.unicast.hosts: ["host1", "host2:port"]' 
network.host: ${host}
EOF
	
	# start elasticsearch
	/usr/local/elasticsearch/bin/elasticsearch  2>&1 &
	echo "/usr/local/elasticsearch/bin/elasticsearch  2>&1 &" >> /etc/rc.local
}

# funtion exec
#ftp_upload
#java_install
elasticsearch_cluster_install cluster-es1
#elasticsearch_cluster_install cluster-es2





