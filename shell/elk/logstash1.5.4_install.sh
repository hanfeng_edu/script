#!/usr/bin/env bash
# ******************************************************
# Filename     :	
# Last modified:	2017-07-14 13:21
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos 6.8
# ******************************************************

ftp_upload() {
       type lftp >/dev/null || yum -y install lftp
       file1="software/source/elk/elasticsearch-1.7.2.tar.gz"
       file2="software/source/jdk/jdk-8u141-linux-x64.gz"
cat >/tmp/ftpcmd.txt<<EOF
open ftp.aishangwei.net:10024
user aishangweidownload aishangwei.net
ls 
mget $file1 
bye
EOF
       lftp -f /tmp/ftpcmd.txt
       rm -r /tmp/ftpcmd.txt
       mv ./$file1 ./$file2  /usr/local/src/
}

java_install(){
    tar xf /usr/local/src/jdk-8u141-linux-x64.gz -C /usr/local/
    ln -sv  /usr/local/jdk1.8.0_141 /usr/local/java
    echo "PATH=/usr/local/java/bin:\$PATH" > /etc/profile.d/java.sh
    . /etc/profile
}

logstash_install(){
    logstashv=logstash-1.5.4
    rediserver=172.16.1.11
    es1=172.16.1.11:9200
    es2=172.16.1.12:9200   
     # install logstash
	tar xf /usr/local/src/${logstashv}.tar.gz -C /usr/local
    ln -sv /usr/local/${logstashv} /usr/local/logstash
    echo "export PATH=\$PATH:/usr/local/logstash/bin" > /etc/profile.d/logstash.sh	
    . /etc/profile 
	
# config send data to elasticsearch
[ ! -d /usr/local/logstash/conf.d ] && mkdir -p /usr/local/logstash/conf.d
cat > /usr/local/logstash/conf.d/logstash_to_elasticsearch.conf <<EOF
input { 
   stdin { } 
}
output {
   elasticsearch {
       host => ["${es1}","${es2}"]
       protocol => "http"
   }
}
EOF
# config send data to redis

    # start logstash
    # /usr/local/logstash/bin/logstash  -f /usr/local/logstash/conf.d/logstash_to_elasticsearch.conf  >> /usr/local/logstash/logs/nohup.out 2>&1 &
}
#java_install
logstash_install
echo "请根据需要自行编写 conf.d/ 下的配置文件"









