#!/usr/bin/env bash
# ******************************************************
# Filename     :	
# Last modified:	2017-07-14 13:21
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos 6.8
# ******************************************************

ftp_upload() {
       type lftp >/dev/null || yum -y install lftp
       file1="software/source/elk/kibana-4.1.2-linux-x64.tar.gz"
cat >/tmp/ftpcmd.txt<<EOF
open ftp.aishangwei.net:10024
user aishangweidownload aishangwei.net
ls 
mget $file1 
bye
EOF
       lftp -f /tmp/ftpcmd.txt
       rm -r /tmp/ftpcmd.txt
       mv ./$file1  /usr/local/src/
}

kibana_install(){
    $host=172.16.1.11
    kibanav=kibana-4.1.2-linux-x64
    tar xf /usr/local/src/${kibanav}.tar.gz -C /usr/local/
	ln -sv /usr/local/${kibanav} /usr/local/kibana
    sed -i "s/localhost/${host}/g"  /usr/local/kibana/config/kibana.yml
	/usr/local/kibana/bin/kibana 2>&1 &
    echo "/usr/local/kibana/bin/kibana 2>&1 &" >> /etc/rc.local
}











