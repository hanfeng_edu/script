#!/usr/bin/env bash
# ******************************************************
# Filename     :	rsync_install.sh
# Last modified:	2017-07-04 09:59
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# install rsync

# 已经在centos6.8测试过
rsync_source(){
RSYNCV="rsync-3.1.2"
tar -xf /usr/local/src/${RSYNCV}.tar.gz -C /usr/local/src/
cd /usr/local/src/${RSYNCV}
./configure --prefix=/usr/local/${RSYNCV} --disable-ipv6
make && make install

# add dir
ln -sv /usr/local/${RSYNCV} /usr/local/rsync
mkdir -pv /usr/local/rsync/{etc,logs,var}

# main config
cat >/usr/local/rsync/etc/rsyncd.conf<<EOF
log file = /usr/local/rsync/logs/rsyncd.log
pid file = /usr/local/rsync/var/rsyncd.pid
lock file = /usr/local/rsync/var/rsyncd.lock
uid = nobody
gid = nobody
usechroot = no
max connections = 30
strict modes = yes

[testcom]
uid = root
gid = root
path = /data/backup/www
read only = no
write only = on
ignore errors
hosts allow = 172.16.1.0/24
hosts deny = *
list = false
comment = www.test.com back
motd file=/usr/local/rsync/etc/rsyncd.motd
secrets file=/usr/local/rsync/etc/rsyncd.passwd
auth users = backuser
EOF

# password file
echo "backuser:abc123" >/usr/local/rsync/etc/rsyncd.passwd
chmod 600 /usr/local/rsync/etc/rsyncd.passwd

# 配置欢迎信息
echo "welcom use rsync-3.1.2" >/usr/local/rsync/etc/rsyncd.motd

# 服务器必须添加配置文件定义的用户
useradd -r -s /sbin/nologin backuser

# 开机自动启动
echo "/usr/local/bin/rsync --daemon --config=/usr/local/rsync/etc/rsyncd.conf" >>/etc/rc.d/rc.local
/usr/local/bin/rsync --daemon --config=/usr/local/rsync/etc/rsyncd.conf
}

rsync_source

# 客户端配置
#echo "abc123" >/etc/rsyncd.passwd
#chmod 600 /etc/rsyncd.passwd
#echo " * 2 * * * root rsync -avH --progress --delete --password-file=/etc/rsyncd.passwd /data/www/www.test.com backuser@172.16.1.12::testcom >> /var/log/rsync.log" >>/etc/crontab







