#!/usr/bin/env bash
# ******************************************************
# Filename     :	redis-3.0.7_install.sh
# Last modified:	2017-07-14 13:21
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

wget http://download.redis.io/releases/redis-3.0.7.tar.gz
redisserver_install(){
    yum -y install tcl

    # install redis
    tar -xf redis-3.0.7.tar.gz -C /usr/local/
    ln -sv /usr/local/{redis-3.0.7,redis}
    cd /usr/local/redis
    make

    sed -i "s/daemonize no/daemonize yes/g"  /usr/local/redis/redis.conf

    # start redis
    /usr/local/redis/src/redis-server  /usr/local/redis/redis.conf
	echo "/usr/local/redis/src/redis-server  /usr/local/redis/redis.conf" >> /etc/rc.local
}

# 启动redis
#/usr/local/redis/src/redis-server ../redis.conf
# 关闭redis
#/usr/local/redis/src/redis-cli shutdown

# 从redis，多下面一项
#sed -i "/# slaveof/ a slaveof 172.16.1.11 6379" /usr/local/redis/redis.conf



