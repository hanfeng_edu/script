#!/usr/bin/env bash
# ******************************************************
# Filename     : zabbix_install.sh
# Last modified: 2017-05-22 11:28
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# install mysql
MYSQLV=mysql5.6.36_glibc_install.sh
wget -P /tmp ftp://test1:test1\!@172.16.1.10/mis/shell/mysql/${MYSQLV}
chmod +x /tmp/${MYSQLV}
/tmp/${MYSQLV}






# install php
PHPV=php-5.6.0_install.sh
wget -P /tmp/ ftp://test1:test1\!@172.16.1.10/mis/shell/php/$PHPV
chmod +x /tmp/$PHPV
/tmp/$PHPV
sed -i '/max_execution_time = / c max_execution_time = 300' /usr/local/php/etc/php.ini 
sed -i '/memory_limit = / c memory_limit = 128M' /usr/local/php/etc/php.ini 
sed -i '/post_max_size = / c post_max_size = 16M' /usr/local/php/etc/php.ini 
sed -i '/upload_max_filesize = / c upload_max_filesize = 2M' /usr/local/php/etc/php.ini 
sed -i '/max_input_time = / c max_input_time = 300' /usr/local/php/etc/php.ini 







