#!/usr/bin/env bash
# ******************************************************
# Filename     : zabbix_agent_install.sh
# Last modified: 2017-05-23 20:59
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

wget=`which wget`
SYS=`uname -r |awk -F "." '{print $4}'`
hostname=`hostname`
wget -P /tmp/  ftp://172.16.1.10/mis/software/rpm/zabbix/zabbix-3.0.9/zabbix-agent-3.0.9-1.${SYS}.x86_64.rpm
rpm -ivh  /tmp/zabbix-agent-3.0.9-1.${SYS}.x86_64.rpm
sed -i "/^Server=/ c Server=172.16.1.11" /etc/zabbix/zabbix_agentd.conf
sed -i "/^ServerActive=/ c ServerActive=172.16.1.11" /etc/zabbix/zabbix_agentd.conf
sed -i "/^Hostname=/ c Hostname=$HOSTNAME" /etc/zabbix/zabbix_agentd.conf
chkconfig --add zabbix-agent
chkconfig zabbix-agent on
service zabbix-agent start
