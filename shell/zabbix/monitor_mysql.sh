#!/usr/bin/env bash
# ******************************************************
# Filename     : monitor_mysql.sh
# Last modified: 2017-05-23 18:53
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************
MYSQLUSER=
MYSQLPWD=
MYSQLPORT=
MYSQLHOST=
mysql=`which mysql`
cp=`which cp`
MYSQLSOCK=find / -name *sock -print |awk '/mysql.sock$/'
MYSQLPARA=find / -name userparameter_mysql.conf
mysql -u$MYSQLUSER -p$MYSQLPWD -P$MYSQLPORT -h$MYSQLHOST -e 'GRANT USAGE ON *.* TO 'zabbixmonitor'@'localhost' BY 'zang123';
mysql -u$MYSQLUSER -p$MYSQLPWD -P$MYSQLPORT -h$MYSQLHOST -e 'FLUSH PRIVILEGES';
cat > /etc/zabbix/.my.cnf <<EOF
[client]
host=localhost
user=zabbixmonitor
password=zang123
socket=$MYSQLSOCK
EOF
cp $MYSQLPARA /etc/zabbix/zabbix_agentd.d/
sed -i 's#/var/lib/zabbix#/etc/zabbix#g' /etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf


