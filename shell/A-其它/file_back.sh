#!/bin/bash
# author:        jack.zang
# date:          2019-11-20
# description:   backup files
##################################################
LogFile=/data/backup/log/`date +"%Y%m"`.log
DATE=`date +"%Y%m%d"`
# 指定要备份的文件，eg：/var/log/slapd.log
BackFileList=/data/backup/back-file-list.txt
BackDir=/data/backup
# 保持多少天备份
RetainDay=30
##################################################

mkdir -p ${LogFile%/*} $BackDir

back_file() {
   echo "[Backup] start at $(date +"%Y-%m-%d %H:%M:%S")" >> $LogFile
   echo "--------------------------------------------------" >> $LogFile
   BACK_FILE_LIST=`cat $BackFileList`

   for BACK_FILE in $BACK_FILE_LIST; do
       FILE=${BACK_FILE##*/}
	   tar -zcPvf $BackDir/${FILE}_$(date +%Y%m%d).tar.gz $BACK_FILE > /dev/null
	   echo "Backup $BACK_FILE done into $BackDir" >> $LogFile
   done
   echo "backup end at $(date +"%Y-%m-%d %H:%M:%S")" >> $LogFile
}

del_old_file() {
   find $BackDir -mtime +${RetainDay} -name "*.tar.gz" -exec rm -rf {} \;
}

back_file
del_old_file