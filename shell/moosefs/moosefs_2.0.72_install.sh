#!/usr/bin/env bash
# ******************************************************
# Filename     :	moosefs_2.0.72_install.sh
# Last modified:	2017-07-12 09:25
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

####### 主配置选项 #######
mfsuser=mfs
mfsgroup=mfs
mfsv=moosefs-2.0.72-1.tar.gz
# mfs 服务脚本
mfsserver=mfs.server


###### end ######



# 添加用户和组
checkuser_exists() {
     # 需要检测的用户名和组
     user=$mfsuser
     group=$mfsgroup
     
     # 判断组是否存在
     egrep "^${group}" /etc/group >& /dev/null
     if [ $? -ne 0 ];then
         groupadd -r ${group}
         echo "system group ${group} is create!------" >> ./install.log
     else  
         echo -e "\033[41m system group ${group} is exist! \033[0m" >> ./install.log
         
     fi
     
     # 判断用户是否存在
     egrep "^${user}" /etc/passwd >& /dev/null
     if [ $? -ne 0 ];then
         useradd -r -g ${group} -s /sbin/nologin ${user}
         echo "system user ${user} is create!------" >> ./install.log
     else  
         echo -e "\033[41m system user ${user} is exist! \033[0m" >> ./install.log 
     fi    

}

# 检测文件是否存在，不存在，就停止执行
checkfile_exist() {
      # 需要检测的文件名
      file[0]=${mfsv}
      file[1]=${mfsserver}

      # 循环检测文件是否存在
      for i in ${file[@]}
      do
          if [ ! -e $i ];then
          echo -e "\033[41m file $i no exist! \033[0m" >>./install.log
          exit 1
          fi
      done

      # 如果执行结果为 1，就退出，不在执行后面命令
      set -e
}

checkuser_exists
checkfile_exist











