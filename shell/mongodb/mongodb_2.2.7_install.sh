#!/usr/bin/env bash
# ******************************************************
# Filename     :	mongodb_2.2.7_install.sh
# Last modified:	2017-07-06 09:24
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************


####### main config start ######
# 软件版本
mdbv=mongodb-linux-x86_64-2.2.7
# 数据库目录
mdbp=/data/mongodb/data
# 日志目录
mdbl=/data/mongodb/logs
# 配置文件目录
mdbc=/usr/local/mongodb/conf.d

###### main config end ######

# install mongodb
tar -zxvf /usr/local/src/${mdbv}.tgz -C /usr/local/
ln -sv /usr/local/${mdbv} /usr/local/mongodb
mkdir -pv ${mdbc}
mkdir -pv ${mdbp} ${mdbl}

# 创建配置文件，默认无配置文件
cp /usr/local/src/mongodb.conf_2.2.7 ${mdbc}/mongodb.conf

# 导入环境变量
echo "export PATH=/usr/local/mongodb/bin:$PATH" >/etc/profile.d/mongodb.sh


# 开机自启
cp /usr/local/src/mongodb_server_2.2.7 /etc/init.d/mongodb
chmod +x /etc/init.d/mongodb
chkconfig --add mongodb
chkconfig mongodb on

# iptables open
iptables -I INPUT -p tcp -m state --state NEW -m tcp --dport 27017 -j ACCEPT
service iptables save
service iptables reload

# 启动mongodb
#service mongodb start
