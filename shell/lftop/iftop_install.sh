#!/usr/bin/env bash
# ******************************************************
# Filename     :	lftop_install.sh
# Last modified:	2017-07-04 16:23
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

yum install flex byacc  libpcap ncurses ncurses-devel libpcap-devel

tar -xf /usr/local/src/iftop-0.17.tar.gz -C /usr/local/src/
cd /usr/local/src/iftop-0.17
./configure --prefix=/usr/local/iftop-0.17
make && make install
