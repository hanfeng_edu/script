
# install depend
echo "--------install depend--------"
yum -y install zlib-devel openssl-devel ImageMagick-devel wget curl-devel rubygems mod_fcgid ruby-devel
# install rvm
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable

# load RVM envir and get need packages
source /etc/profile.d/rvm.sh
rvm requirements

# use rvm install ruby 2.2.3 , sed default
sed -i -E 's!https?://cache.ruby-lang.org/pub/ruby!https://ruby.taobao.org/mirrors/ruby!' /usr/local/rvm/config/db
rvm gemset create
rvm install 2.2.3
# reinstall ruby
# rvm install 2.2.3 --create
rvm use 2.2.3 --default

# install rails
#gem sources -l   #查看镜像站
gem sources --remove https://rubygems.org/
gem sources -a https://ruby.taobao.org/
gem update --system       #更新源
gem install rails -v=4.2.8

# install mysql and httpd
yum install -y httpd httpd-devel mariadb mariadb-server mariadb-devel

#mariadb-server init
mysql_secure_installation
systemctl start mariadb
systemctl enable mariadb
mysql> create database redmine character set utf8;
mysql> create user 'redmine'@'localhost' identified by 'redmine';
mysql> grant all privileges on redmine.* to 'redmine'@'localhost';

# install apache of redmine
gem install passenger
passenger-install-apache2-module

# 模块位置一定要根据自己的实际目录查看一下，看是否正确，否则无法正常启动http
vim /etc/httpd/conf.d/passenger.conf
LoadModule passenger_module /usr/local/rvm/gems/ruby-2.2.3/gems/passenger-5.0.30/buildout/apache2/mod_passenger.so
<IfModule mod_passenger.c>
	PassengerRoot /usr/local/rvm/gems/ruby-2.2.3/gems/passenger-5.0.30
	PassengerDefaultRuby /usr/local/rvm/gems/ruby-2.2.3/wrappers/ruby
</IfModule>

vim /etc/httpd/conf.d/redmine.conf
<VirtualHost *:80>
	ServerName www.a.com
	# !!! Be sure to point DocumentRoot to 'public'!
	DocumentRoot /var/www/html/redmine/public
ErrorLog logs/redmine_error_log
    <Directory /var/www/html/redmine/public>
		Options Indexes ExecCGI FollowSymLinks
		Order allow,deny
		Allow from all
		# This relaxes Apache security settings.
		AllowOverride all
		# MultiViews must be turned off.
		Options -MultiViews
		# Uncomment this if you're on Apache >= 2.4:
		#Require all granted
    </Directory>
</VirtualHost>

# install redmine
cd /var/www/html
tar xf redmine-3.4.2.tar.gz
mv redmine-3.4.2 redmine
cd /var/www/html/redmine/

vim Gemfile
#source 'https://rubygems.org'
source'https://ruby.taobao.org'

cp config/configuration.yml{.example,}
cp config/database.yml{.example,}

vim config/database.yml
production:
  adapter: mysql2
  database: redmine
  host: localhost
  username: redmine
  password: "redmine"
  encoding: utf8

# install ruby denpend tool  (bundler)
gem install bundler
bundle install
# 如果http无法正常启动的话，该步骤无法正常使用
rake generate_secret_token

# init redmine database tablename
RAILS_ENV=production rake db:migrate
RAILS_ENV=production rake redmine:load_default_data

# modify fastcgi
cd /var/www/html/redmine/public
cp dispath.fcgi{.example,}
cp htaccess.fcgi.example .htaccess

# 在 WEBrick服务上测试 Redmine是否安装成功
#bundle exec rails server webrick -e production -b 0.0.0.0

# default user and passwd(admin@admin)


# SCM Creator plugin install(该插件兼容redmine2.5.2)
cd /var/www/html/redmine/plugins
tar xf redmine_scm-0.5.0b.tar.bz2
chown -R jack:jack redmine_scm
rake redmine:plugins:migrate RAILS_ENV=production
systemctl restart httpd





















