#!/usr/bin/env bash
# ******************************************************
# Filename     :	tomcat_8.0.44_install.sh
# Last modified:	2017-07-26 23:17
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# instance 个数
INSTANCEN=2


# 函数检测文件是否存在
checkfile_exist() {
      # 需要检测的文件名
      file[1]=jdk-8u141-linux-x64.gz
      file[2]=tomcat.sh
      file[3]=apache-tomcat-8.0.44.tar.gz


      # 循环检测文件是否存在
      for i in ${file[@]}
      do
          if [ ! -e /usr/local/src/$i ];then
          echo -e "\033[41m --$i no exist!--- -- \033[0m"
          exit 1
		  fi	  
      done

      # 如果执行结果为 1，就退出，不在执行后面命令
      set -e
}
checkfile_exist

# 安装jdk
tar xzf /usr/local/src/jdk-8u141-linux-x64.gz -C /usr/local/
ln -sv /usr/local/{jdk1.8.0_141,java}
cat >/etc/profile.d/java.sh <<EOF
export JAVA_HOME=/usr/local/java
export PATH=\${JAVA_HOME}/bin:\$PATH
EOF
source /etc/profile.d/java.sh

# 安装tomcat
tar -xf /usr/local/src/apache-tomcat-8.0.44.tar.gz -C /usr/local/
ln -sv /usr/local/{apache-tomcat-8.0.44,tomcat}
cd /usr/local/tomcat
mkdir instance.bak
mv conf logs webapps temp work instance.bak/

# 制作tomcat启动脚本

N=1
while (( $N <= $INSTANCEN ))
do
mkdir  instance${N}
cp -r /usr/local/tomcat/instance.bak/*  instance${N}/

# 配置server.xml
cp /usr/local/src/tomcat.sh /usr/local/tomcat/instance${N}
chmod +x /usr/local/tomcat/instance${N}/tomcat.sh
let "N++"
done















