#!/usr/bin/env bash
# ******************************************************
# Filename     :	mysql_data_backup.sh
# Last modified:	2017-07-26 19:29
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

ip=`awk '/IPADDR/' /etc/sysconfig/network-scripts/ifcfg-eth0 |awk -F "=" '{print $2}' `

# config backup
mysql_bk_directory=/data/backup/mysql/${ip}
mysql_cnf_file='/etc/my.cnf'
mysql_password='hqsZO5XQ'

# config sendmail
maillist="zangxuefeng@rshui.cn"

dateb=`date +%Y%m%d`
mysql_back(){
        # create directory for backup
        [ ! -d ${mysql_bk_directory} ] && mkdir -p ${mysql_bk_directory}
        # backup my.cnf
        tar zcf ${mysql_bk_directory}/mycnf_${ip}_${dateb}.tar.gz ${mysql_cnf_file} >/dev/null 2>&1
        # backup mysql_data
        mysqldump -uroot -p${mysql_password} --master-data=2 --ignore-table=mysql.event --lock-all-tables -B dataname1 dataname2 dataname3 |gzip >${mysql_bk_directory}/mysql_${ip}_${dateb}.sql.gz

        # del old backup
        findf=$(find ${mysql_bk_directory} -name "*.gz" -mtime +30 -o -name "*.log" -mtime +30) 
		if [ -n ${findf}];then
		    rm -f $findf
			echo  "succeed--`date +%F-%R` delete backup $findf succeed." >> ${mysql_bk_directory}/mysql_back_${dateb}.log
        fi
        # detection if the backup is successful ?
        if [ -e ${mysql_bk_directory}/mycnf_${ip}_${dateb}.tar.gz ];then
            echo "succeed--`date +%F-%R` backup mycnf_${ip}_${dateb}.tar.gz succeed." >> ${mysql_bk_directory}/mysql_back_${dateb}.log
        else
            echo "failed--`date +%F-%R` backup mycnf_${ip}_${dateb}.tar.gz failed!" >> ${mysql_bk_directory}/mysql_back_${dateb}.log
        fi

        if [ -e ${mysql_bk_directory}/mysql_${ip}_${dateb}.sql.gz ];then
           echo "succeed--`date +%F-%R` backup mysql_${ip}_${dateb}.sql.gz succeed." >> ${mysql_bk_directory}/mysql_back_${dateb}.log
        else
           echo "failed--`date +%F-%R` backup mysql_${ip}_${dateb}.sql.gz failed!" >> ${mysql_bk_directory}/mysql_back_${dateb}.log
        fi
} 
 
# 发送邮件
mail_to() { 
     mailx -s "系统备份日志" -r ${ip}@aishangwei.net  ${maillist} < ${mysql_bk_directory}/mysql_back_${dateb}.log
}

# ftp上传
ftp_upload() {
     dateb=`date +%Y%m%d`
	 ### 上传文件的目录
     uploadpath[1]="${mysql_bk_directory}"
	 #uploadpath[2]=""
     ftpip="103.244.234.150:21"
     ftpuser="chushang"
     ftppasswd="bn<67^adt6"

type lftp 1>/dev/null 2>/dev/null || yum -y install lftp
     for p in ${uploadpath[@]}
     do
	     ### 过滤要上传的文件 
         uploadfile="`ls $p |grep $dateb`"

         for f in $uploadfile
         do
cat >/tmp/ftpcmd.txt<<EOF
open $ftpip
user "${ftpuser}" "${ftppasswd}"
mput $p/$f
bye
EOF
         lftp -f /tmp/ftpcmd.txt
         done
     done
rm -f /tmp/ftpcmd.txt
}


echo "" > ${mysql_bk_directory}/mysql_back_${dateb}.log
mysql_back
#mail_to
ftp_upload














