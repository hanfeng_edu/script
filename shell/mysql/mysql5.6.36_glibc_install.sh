#!/usr/bin/env bash
# ******************************************************
# Filename     : mysql5.6.36_glibc_install.sh
# Last modified: 2017-06-21 19:14
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : 
# ******************************************************

# mysql version
MYSQLV=mysql-5.6.36-linux-glibc2.5-x86_64
# mysql install path
MIP=/usr/local
#mysql data path
DATAP=/data/mysql

#wget -P /usr/local/src/ ftp://test1:test1\!@172.16.1.10/mis/software/source/mysql/${MYSQLV}.tar.gz


# 函数检测文件是否存在
checkfile_exist() {
      # 需要检测的文件名
      file[1]=mysql-5.6.36-linux-glibc2.5-x86_64.tar.gz
	  file[2]=mysql_5.6.36.my.cnf

      # 循环检测文件是否存在
      for i in ${file[@]}
      do
          if [ ! -e /usr/local/src/$i ];then
          echo -e "\033[41m --$i no exist!---\n ---Start download $i -- \033[0m"
          fi
      done

      # 如果执行结果为 1，就退出，不在执行后面命令
      set -e
}
checkfile_exist

yum -y install perl perl-devel autoconf 

groupadd -r mysql
useradd -r -g mysql -s /sbin/nologin -M mysql
mkdir $DATAP/logs -pv
chown -R mysql:mysql $DATAP

tar xf /usr/local/src/${MYSQLV}.tar.gz -C ${MIP}/
ln -sv /usr/local/${MYSQLV} /usr/local/mysql
cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql

chmod +x /etc/init.d/mysql
chkconfig --add mysql
chkconfig mysql on

#wget -P /etc/my.cnf ftp://test1:test2\!@172.16.1.10/mis/shell/mysql/conf.d/my.cnf
rm -rf /etc/my.cnf
cp -f  /usr/local/src/mysql_5.6.36.my.cnf /etc/my.cnf


cat >/etc/profile.d/mysql.sh <<EOF
export MYSQL_HOME=/usr/local/mysql
export PATH=\${MYSQL_HOME}/bin:\$PATH
EOF
source /etc/profile.d/mysql.sh

/usr/local/mysql/scripts/mysql_install_db --user=mysql --basedir=/usr/local/mysql --datadir=/data/mysql
service mysql start












