#!/usr/bin/env bash
# ******************************************************
# Filename     :	
# Last modified:	2017-07-14 13:21
# Version      : 
# Author       : jack.zang
# Email        : jack.zang@aishangwei.net
# Description  : for centos 6.8
# ******************************************************

# config start
mysql_user=''
mysql_passwd=''
# backup single databasename
sdata=''
# backup dir path
#dirp=/data/www/h5game
# config end
dirp=''

mysql_backup(){
    if [ ! ${mysql_user}="" ] && [ ! ${mysql_passwd}="" ] && [ ! ${sdata}="" ] ;then
         echo -e "\033[41m --mysql user or passwd or single dataname is null,please setting!----- \033[0m"
         exit 1
         set -e
    fi
    ip=`awk '/IPADDR/' /etc/sysconfig/network-scripts/ifcfg-eth0 |awk -F "=" '{print $2}' `
    # backup all databases
    mysqldump -u$mysql_user -p${mysql_passwd} --master-data=2 --ignore-table=mysql.event --lock-all-tables -A -B |gzip >./mysqlall_${ip}_$(date +%F-%H.%M).sql.gz
    # backup single database
    mysqldump -uroot -p${mysql_passwd} -B ${sdata} >./mysql${sdata}_${ip}_$(date +%F-%H.%M).sql
}

dir_backup(){
     if [ ! ${dirp}="" ] ;then
         echo -e "\033[41m --backup dir is not set,please setting!----- \033[0m"
         exit 1
         set -e
     fi 
     dirn=`echo ${dirp} |awk -F '/' '{print $NF}'` 
     cd $dirp
     tar -zcpf ~/${dirn}.tar.gz  ./*
}

#mysql_backup
#dir_backup











